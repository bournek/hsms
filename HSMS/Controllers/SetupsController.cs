﻿namespace com.bkss.Controllers
{
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Boilerplate.AspNetCore;
    using Boilerplate.AspNetCore.Filters;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Options;
    using com.bkss.Constants;
    using com.bkss.Services;
    using com.bkss.Settings;
    using Microsoft.AspNetCore.Authorization;
    using System;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.AspNetCore.Identity;
    using IdentityModels;
    using ViewModels;
    using System.Linq;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Data;
    using System.Dynamic;
    using Microsoft.Net.Http.Headers;
    using System.IO;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Newtonsoft.Json.Linq;
    using Newtonsoft.Json;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using System.Drawing;

    [Authorize]
    public class SetupsController : Controller
    {
        #region Fields

        private readonly IServiceProvider serviceProvider;
        private readonly IOptions<AppSettings> appSettings;
        private readonly IBrowserConfigService browserConfigService;
#if NET461
        // The FeedService is not available for .NET Core because the System.ServiceModel.Syndication.SyndicationFeed
        // type does not yet exist. See https://github.com/dotnet/wcf/issues/76.
        private readonly IFeedService feedService;
#endif
        private readonly IManifestService manifestService;
        private readonly IOpenSearchService openSearchService;
        private readonly IRobotsService robotsService;
        private readonly ISitemapService sitemapService;
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly AppCache appCache;
        #endregion

        #region Constructors

        public SetupsController(
            IBrowserConfigService browserConfigService,
#if NET461
            // The FeedService is not available for .NET Core because the System.ServiceModel.Syndication.SyndicationFeed
            // type does not yet exist. See https://github.com/dotnet/wcf/issues/76.
            IFeedService feedService,
#endif
            IManifestService manifestService,
            IOpenSearchService openSearchService,
            IRobotsService robotsService,
            ISitemapService sitemapService,
            IServiceProvider provider,
            IAppCacheService cache,
             IHostingEnvironment env,
            UserManager<ApplicationUser> userManager,
            IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings;
            this.browserConfigService = browserConfigService;
#if NET461
            // The FeedService is not available for .NET Core because the System.ServiceModel.Syndication.SyndicationFeed
            // type does not yet exist. See https://github.com/dotnet/wcf/issues/76.
            this.feedService = feedService;
#endif
            this.manifestService = manifestService;
            this.openSearchService = openSearchService;
            this.robotsService = robotsService;
            this.sitemapService = sitemapService;
            this.serviceProvider = provider;
            this._userManager = userManager;
            this.hostingEnvironment = env;
            //
            this.appCache = (AppCache)cache;

            if(appCache.SchoolId == 0)
            {
                //Choose school
                RedirectToRoute(SetupsControllerRoute.GetIndex,new {m=2 });
            }
        }

        #endregion

        [HttpGet("setups", Name = SetupsControllerRoute.GetIndex)]
        public async Task<IActionResult> Index(int m, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            //var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

            ViewBag.Mode = m;
            //
            if (!db.Accounts.Any() || !db.Clients.Any())
            {
                return RedirectToRoute(HomeControllerRoute.GetIndex);
            }
            //
            if(Request.Query.Any(p=>p.Key == "msg"))
            {
                ViewBag.Message = Request.Query.Single(p => p.Key == "msg").Value;
            }else if(Request.Query.Any(p => p.Key == "err"))
            {
                ViewBag.Warning = Request.Query.Single(p => p.Key == "err").Value;
            }
            //
            if (Request.Query.Any(p => p.Key == "i"))
            {
                try
                {
                    ViewBag.Index = int.Parse(Request.Query.Single(p => p.Key == "i").Value);
                }
                catch { }
            }
            //
            //

            if (appCache.SchoolId == 0 && db.Schools.Include(p => p.CountrySchool).Any(o => o.CountrySchool.Name.Equals(appSettings.Value.Profile.Name)))
            {
                //
                var sch = db.Schools.Include(p => p.CountrySchool).SingleOrDefault(o => o.CountrySchool.Name.Equals(appSettings.Value.Profile.Name));
                if(sch != null)
                {
                    ViewBag.SchoolId = sch.Id;
                    ViewBag.School = sch.CountrySchool.Name;
                    //
                    appCache.SchoolId = sch.Id;
                }
            }
            //var q = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Schools'";
            //IEnumerable<string> columns = db.Set().FromSql(q);
            //using (var connection = db.Database.GetDbConnection())
            //{
            //    connection.Open();

            //    using (var command = connection.CreateCommand())
            //    {
            //        command.CommandText = q;
            //        var reader = command.ExecuteReader();
            //        var rowList = new List<object>();
            //        if (reader.HasRows)
            //        {
            //            while (reader.Read())
            //            {
            //                if (reader.FieldCount > 0)
            //                {
            //                    var values = new object[reader.FieldCount];
            //                    reader.GetValues(values);
            //                    // Add the array to the ArrayList
            //                    rowList.Add(values);
            //                }
            //                //reader.NextResult();
            //            }
            //        }
            //        else
            //        {
            //            Console.WriteLine("No rows found.");
            //        }
            //        //

            //        System.Diagnostics.Debug.WriteLine("Found  "+rowList.ToString());
            //        //var result = command.ExecuteScalar().ToString();
            //    }
            //}
            //ViewData["user"] = await userManager.FindByNameAsync(User.Identity.Name);
            return this.View(SetupsControllerAction.Index);
        }
        //Configure School
        [HttpPost("setups", Name = SetupsControllerRoute.GetIndex)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(AddSchoolViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if(model.School > 0)
            {
                //var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                //var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //
                if (appCache.SchoolId == 0)
                    appCache.SchoolId = model.School;
                //
                return RedirectToRoute(SetupsControllerRoute.GetStart);
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Index);
        }
        //Choose School
        [HttpGet("chooseschool", Name = SetupsControllerRoute.ChooseSchool)]
        public async Task<IActionResult> ChooseSchool(int m, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;

            if (appCache.SchoolId > 0)
                return Redirect(returnUrl);
            //
            ViewBag.ReturenUrl = returnUrl;
            //
            return View(SetupsControllerAction.ChooseSchool);
        }
        [HttpPost("chooseschool", Name = SetupsControllerRoute.ChooseSchool)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChooseSchool(AddSchoolViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if (model.School > 0)
            {
                //var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                //var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //
                if (appCache.SchoolId == 0)
                    appCache.SchoolId = model.School;
                //
                if(returnUrl != null)
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToRoute(SetupsControllerRoute.GetStart);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.ChooseSchool);
        }
        //
        // GET: /Setups/Schools/
        [HttpGet("start", Name = SetupsControllerRoute.GetStart)]
        public IActionResult Schools(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            if (Request.Query.Any(p => p.Key == "msg"))
            {
                ViewBag.Message = Request.Query.Single(p => p.Key == "msg").Value;
            }
            //
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
            //

            if (db.Schools.Include(p => p.CountrySchool).Any(o => o.CountrySchool.Name.Equals(appSettings.Value.Profile.Name)))
            {
                //
                var school = db.Schools.Include(p => p.CountrySchool).SingleOrDefault(o => o.CountrySchool.Name.Equals(appSettings.Value.Profile.Name));

                ViewBag.SchoolId = school?.Id;
                ViewBag.School = school?.CountrySchool.Name;
            }
            //
            return View(SetupsControllerAction.Start);
        }

        [HttpPost("start", Name = SetupsControllerRoute.GetStart)]
        [ValidateAntiForgeryToken]
        public IActionResult FinishSchools(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

            if (db.Years.Count() == 0)
            {
                //
                return RedirectToRoute(SetupsControllerRoute.GetAcademics, new { msg = "No Academic years have been configured in the system." });
            }else if(db.Terms.Count() == 0)
            {
                //
                return RedirectToRoute(SetupsControllerRoute.GetAcademics, new { msg = "No Academic terms have been configured in the system." });
            }
            else if (db.Subjects.Count() == 0)
            {
                //
                return RedirectToRoute(SetupsControllerRoute.GetAcademics, new { msg = "No Academic subjects have been configured in the system." });
            }
            //
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Account/Login
        [HttpPost("addschool", Name = SetupsControllerRoute.AddSchool)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSchool(AddSchoolViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if (ModelState.IsValid)
            {
                //
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

                try
                {
                    var countrySchool = db.AllSchools.Single(p => p.Id == model.School);
                    countrySchool.Motto = model.Motto;
                    countrySchool.ContactEmail = model.Email;
                    countrySchool.County = model.County;
                    countrySchool.City = model.City;
                    countrySchool.Street = model.Street;
                    countrySchool.PostalCode = model.PostalCode;
                    //
                    if (model.WorkPhone != null)
                        countrySchool.WorkPhone = model.WorkPhone;
                    if (model.MobilePhone != null)
                        countrySchool.MobilePhone = model.MobilePhone;
                    //
                    await db.SaveChangesAsync();

                    if (!db.Schools.Include(l => l.CountrySchool).Any(p => p.CountrySchool.Id == countrySchool.Id))
                    {
                        var client = db.Clients.First();
                        //
                        var school = new School();
                        school.CountrySchool = countrySchool;
                        school.Client = client;
                        //
                        db.Schools.Add(school);
                        //
                        await db.SaveChangesAsync();

                        //
                        return RedirectToRoute(SetupsControllerRoute.ChooseSchool);
                    }else
                    {
                        ModelState.AddModelError("", "Add school failed. A simillar school already exists.");
                    }
                    if (db.Departments.Count() == 0)
                    {
                        ViewBag.Message = "No departments have been found in the system!!";
                    }
                    //
                    ViewBag.Index = 1;
                }
                catch
                {
                    ModelState.AddModelError("", "Add school failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }

        [HttpGet("addlevel", Name = SetupsControllerRoute.AddLevel)]
        public async Task<IActionResult> AddLevel(string returnUrl = null,string msg = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if(appCache.GetSchool() != null)
                ViewBag.School = appCache.GetSchool();
            //
            ViewBag.Message = msg;
            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }
        // POST: /Setups/AddLevel
        [HttpPost("addlevel", Name = SetupsControllerRoute.AddLevel)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddLevel(AddLevelViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

                var school = db.Schools.Include(p => p.CountrySchool).Include(l => l.Levels).Single(p => p.Id == appCache.SchoolId);

                if (!school.Levels.Any(p => p.Name.ToLower().Equals(model.Name.ToLower()) && p.Value == model.Level))
                {
                    try
                    {
                        //
                        var lev = new AcademicLevel();
                        lev.Name = model.Name;
                        lev.Value = model.Level;
                        lev.SchoolId = school.Id;

                        school.Levels.Add(lev);
                        //
                        await db.SaveChangesAsync();

                        ViewBag.Message = "Level '" + model.Name + "' was added succesifuly to school '" + school.CountrySchool.Name + "'";
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Add Academic level failed.");

                    }
                }
                else
                {
                    //Level already added
                    ViewBag.Warning = "Level '" + model.Name + "(" + model.Level + ")" + "' already exists in school '" + school.CountrySchool.Name + "'";
                }

            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }

        //Streams
        [HttpGet("addstream", Name = SetupsControllerRoute.AddStream)]
        public async Task<IActionResult> AddStream(string returnUrl = null, string msg = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 2;
            if (appCache.GetSchool() != null)
                ViewBag.School = appCache.GetSchool();
            //
            ViewBag.Message = msg;

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }
        [HttpPost("addstream", Name = SetupsControllerRoute.AddStream)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddStream(AddStreamViewModel model, string returnUrl = null)
        {
            ViewBag.Index = 2;
            if (ModelState.IsValid)
            {
                //
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //

                try
                {

                    var school = db.Schools.Include(p => p.Streams).Include(p => p.CountrySchool).Single(p => p.Id == appCache.SchoolId);
                    if (!db.Schools.Any(p => p.Id == appCache.SchoolId && p.Streams.Any(k => k.Name.ToLower().Trim() == model.StreamName.ToLower().Trim())))
                    {

                        var stream = new SchoolStream();
                        stream.Name = model.StreamName;
                        stream.School = school;

                        //
                        school.Streams.Add(stream);
                        //
                        await db.SaveChangesAsync();

                    }
                    if (school.Departments.Where(k => k.Classrooms.Count > 0).Count() == 0)
                    {
                        ViewBag.Message = "No classes have been have been found in the system for this school!!";
                    }
                    //
                    ViewBag.SchoolId = school?.Id;
                    ViewBag.School = school?.CountrySchool?.Name;
                    //
                    //ViewBag.Index = 3;
                }
                catch
                {
                    ModelState.AddModelError("", "Add stream failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }

        //Depts
        [HttpGet("adddept", Name = SetupsControllerRoute.AddDepartment)]
        public async Task<IActionResult> AddDepartment(string returnUrl = null, string msg = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 1;
            if (appCache.GetSchool() != null)
                ViewBag.School = appCache.GetSchool();

            //
            ViewBag.Message = msg;

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }
        // POST: /Account/AddDept
        [HttpPost("adddept", Name = SetupsControllerRoute.AddDepartment)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddDepartment(AddDepartmentViewModel model, string returnUrl = null)
        {
            ViewBag.Index = 1;
            if (ModelState.IsValid)
            {
                //
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //

                try
                {
                    var school = db.Schools.Include(l=>l.CountrySchool).Single(p => p.Id == appCache.SchoolId);
                    if (!db.Departments.Any(p => p.Name.ToLower().Equals(model.DepartmentName.ToLower().Trim())))
                    {

                        var dept = new SchoolDepartment();
                        dept.Name = model.DepartmentName;
                        dept.School = school;

                        switch (model.Type)
                        {
                            case (int)DepartmentType.Welfare:
                                //
                                dept.Type = DepartmentType.Welfare;
                                break;
                            case (int)DepartmentType.Adminstration:
                                //
                                dept.Type = DepartmentType.Adminstration;
                                break;
                            default:
                                //
                                dept.Type = DepartmentType.Academic;
                                break;
                        }
                        //
                        db.Departments.Add(dept);
                        //
                        await db.SaveChangesAsync();
                    }
                    if (school.Streams.Count() == 0)
                    {
                        ViewBag.Message = "No streams have been found in the system!!";
                    }
                    //
                    ViewBag.SchoolId = school?.Id;
                    ViewBag.School = school?.CountrySchool.Name;
                    //
                    //ViewBag.Index = 2;
                }
                catch
                {
                    ModelState.AddModelError("", "Add department failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }

        //Klasses
        [HttpGet("addclass", Name = SetupsControllerRoute.AddClassroom)]
        public async Task<IActionResult> AddClass(string returnUrl = null, string msg = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 3;
            if (appCache.GetSchool() != null)
                ViewBag.School = appCache.GetSchool();

            //
            ViewBag.Message  = msg;
            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }
        // POST: /Setups/AddClass
        [HttpPost("addclass", Name = SetupsControllerRoute.AddClassroom)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddClassroom(AddClassViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 3;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //
                try
                {
                    var dept = db.Departments.Include(i => i.School).Single(p => p.Id == model.Department);

                    var school = db.Schools.Include(i => i.Streams).Single(p => p.Id == dept.SchoolId);
                    var stream = school.Streams.Single(p => p.Id == model.Stream);
                    //Ensure one stream for each dept has one class only
                    if(!db.Classrooms.Any(l=>l.Name.Equals(model.Name) && l.DepartmentId == model.Department &&l.Stream.Name.Equals(stream.Name)))
                    {
                        var cl = new SchoolClass();
                        cl.Department = dept;
                        cl.Stream = stream;
                        cl.Name = model.Name;
                        //
                        db.Classrooms.Add(cl);
                        await db.SaveChangesAsync();
                    }else
                    {
                        //
                        ViewBag.Message = "This classroom already exist.";
                        ModelState.AddModelError("", "Add classroom failed.");
                    }
                }
                catch {
                    ModelState.AddModelError("", "Add classroom failed.");
                }
                //next
                //ViewBag.Index = 4;
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools, model);
        }

        //Offices
        [HttpGet("addoffice", Name = SetupsControllerRoute.AddOffice)]
        public async Task<IActionResult> AddOffice(string returnUrl = null, string msg = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 4;
            if (appCache.GetSchool() != null)
                ViewBag.School = appCache.GetSchool();

            //
            ViewBag.Message = msg;
            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools);
        }
        // POST: /Setups/AddClass
        [HttpPost("addoffice", Name = SetupsControllerRoute.AddOffice)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOffice(AddOfficeViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 4;
            if (ModelState.IsValid)
            {
                //get user name
                try {
                    //
                    var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                    var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                    //

                    var dept = db.Departments.Include(l=>l.School).Single(p => p.Id == model.Department);
                    var office = new SchoolOffice();
                    office.Name = model.Name;
                    office.Department = dept;

                    db.Offices.Add(office);

                    await db.SaveChangesAsync();
                } catch {
                    ModelState.AddModelError("", "Add office failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Schools, model);
        }




        //
        // GET: /Setups/Tution/
        [HttpGet("Academics", Name = SetupsControllerRoute.GetAcademics)]
        public IActionResult Tution(string returnUrl = null, string msg = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            if (Request.Query.Any(p => p.Key == "msg"))
            {
                //ViewBag.Message = Request.Query.Single(p => p.Key == "msg").Value;
            }
            ViewBag.Message = msg;
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/Tution/
        [HttpPost("Academics", Name = SetupsControllerRoute.GetAcademics)]
        [ValidateAntiForgeryToken]
        public IActionResult FinishTution(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

            if (db.Staffs.Count() < 2)
            {
                return RedirectToRoute(SetupsControllerRoute.GetStaff,new {msg="No staff have been configured in the system." });
            }
            return View(SetupsControllerAction.Tution);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        /// POST: /Setups/AddYear
        [HttpPost("addyear", Name = SetupsControllerRoute.AddYear)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddYear(AddYearViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 0;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {

                    //
                    var school = db.Schools.Include(p => p.CountrySchool).Single(p => p.Id == appCache.SchoolId);
                    if (db.Years.Any(p => p.Name.Equals(model.Name) && (p.Starts.CompareTo(model.Starts) >= 0 && p.Ends.CompareTo(model.Ends) <= 0)))
                    {
                        //
                        ViewBag.Warning = "A simillar year within this calendar time-span exists in school '"+school.CountrySchool.Name+"'. Change the name or specify a deferent time-span or edit the ";
                    } else if(db.Years.Any(p=>p.Starts.CompareTo(model.Starts) >= 0 && p.Ends.CompareTo(model.Ends) <= 0)) {
                        //
                    } else {
                        //
                        var yr = new AcademicYear();
                        yr.Name = model.Name;
                        yr.Starts = model.Starts;
                        yr.Ends = model.Ends;

                        yr.School = school;

                        db.Years.Add(yr);

                        //
                        await db.SaveChangesAsync();
                        ViewBag.Message = "Year '" + yr.Name + "' was added succesifuly to school '" + school.CountrySchool.Name + "'";
                    }
                }
                catch {
                    ModelState.AddModelError("", "Add Academic year failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/AddTerm
        [HttpPost("addterm", Name = SetupsControllerRoute.AddTerm)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTerm(AddTermViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 2;
            ViewBag.Year = model.Year;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {
                    //

                    var year = db.Years.Include(k=>k.Terms).Single(p => p.Id == model.Year);
                    foreach(var p in year.Terms)
                    {
                        if(p.Name.Equals(model.Name) && (p.Starts.CompareTo(model.Starts) >= 0 && p.Ends.CompareTo(model.Ends) <= 0)){
                            //
                            ViewBag.Warning = "A simillar term within this calendar time-span exists in year '" + year.Name + "'. Change the name or specify a deferent time-span or edit the ";
                            return View(SetupsControllerAction.Tution);
                        }
                        else if(p.Starts.CompareTo(model.Starts) >= 0 && p.Ends.CompareTo(model.Ends) <= 0){
                            ////this duration is scoped in an existing term
                            ViewBag.Warning = "The specified calendar time-span is covered by an existing Academic term in year '" + year.Name + "'.";
                            return View(SetupsControllerAction.Tution);
                        }
                    }
                    //Continue
                    //
                    var term = new AcademicTerm();
                    term.Ends = model.Ends;
                    term.Name = model.Name;
                    term.Starts = model.Starts;

                    //
                    year.Terms.Add(term);
                    await db.SaveChangesAsync();
                    ViewBag.Message = "Term '" + term.Name + "' was added succesifuly to year '" + year.Name + "'";

                }
                catch {
                    ModelState.AddModelError("", "Add Academic term failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/AddExamType
        [HttpPost("addtype", Name = SetupsControllerRoute.AddExamType)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExamType(AddExamTypeViewModel model, string returnUrl = null, params string[] SelectedExams)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 3;
            ViewBag.Mode = 1;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

                var conv = 1.0;

                if (model.Conversion.Contains('/')) {
                    var c = model.Conversion.Split('/');
                    if (c.Count() > 2)
                    {
                        //ERROR
                        ModelState.AddModelError("", "Bad Conversion unit for exam marks");
                        return View(SetupsControllerAction.Tution);
                    }

                    //
                    try
                    {
                        double x = double.Parse(c[0]); int y = int.Parse(c[1]);

                        if (x<=0)
                        {
                            ModelState.AddModelError("", "Exam conversion ratio cannot be more than '1.0'");
                            return View(SetupsControllerAction.Tution);
                        }
                        conv = x / y;

                        if(conv == 0 || conv > 1)
                        {
                            //
                            ModelState.AddModelError("", "Exam conversion ratio cannot be more than '1.0'");
                            return View(SetupsControllerAction.Tution);
                        }
                        //We Good
                    }
                    catch
                    {
                        //ERROR
                        ModelState.AddModelError("", "Bad Conversion unit for exam marks");
                        return View(SetupsControllerAction.Tution);
                    }
                }
                else
                {
                    try
                    {
                        conv = double.Parse(model.Conversion);
                    }
                    catch
                    {
                        //ERROR
                        ModelState.AddModelError("", "Bad Conversion unit for exam marks");
                        return View(SetupsControllerAction.Tution);
                    }
                }
                //
                var school = db.Schools.Include(i=>i.ExamTypes).Single(p => p.Id == appCache.SchoolId);

                if (!school.ExamTypes.Any(l => l.Name.ToLower().Equals(model.Name.ToLower())))
                {
                    try
                    {
                        //
                        var type = new ExamType();
                        type.Name = model.Name;
                        type.TotalScore = model.Total;
                        type.ConversionConstant = conv;
                        //
                        type.SchoolId = school.Id;
                        school.ExamTypes.Add(type);

                        await db.SaveChangesAsync();
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Add exam type failed.");
                    }
                }else
                {
                    //Already exists
                    ViewBag.Message = "A exam type '" + model.Name +" already exists for this subject.";
                }
                
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/AddExamGrade
        [HttpPost("addgrade", Name = SetupsControllerRoute.AddExamGrade)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExamGrade(AddExamGradeViewModel model, string returnUrl = null, params int[] SelectedLevels)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 3;
            ViewBag.Mode = 2;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {
                    var subject = db.Subjects.Include(o => o.Grades).Include(k=>k.School.CountrySchool).Single(l => l.Id == model.Subject);

                    if (SelectedLevels.Count() == 0)
                    {
                        ModelState.AddModelError("", "Please specify levels where this grading should be applied.");
                        return View(SetupsControllerAction.Tution);
                    }
                    //
                    //var subjects = db.Subjects.Include(p => p.Level).Where(p =>p.Id==model.Subject && (SelectedLevels.Contains(p.Level.Id))).ToList();
                    //
                    if(subject.Grades.Any(l=>l.Label == model.Name && l.MinScore != model.MinMarks))
                    {
                        //update
                        var grade = db.Grades.Include(l => l.Levels).Single(l => l.Label == model.Name);
                        grade.MinScore = model.MinMarks;
                        //
                        if (grade.Remarks != model.Comments)
                        {
                            grade.Remarks = model.Comments;
                        }
                        //Remove old
                        var toRemove = new List<int>();
                        foreach(var n in grade.Levels)
                        {
                            if(!SelectedLevels.Any(p=>p == n.Id))
                            {
                                toRemove.Add(n.Id);
                            }
                        }
                        //Add new
                        foreach(var k in SelectedLevels)
                        {
                            if(!grade.Levels.Any(p=>p.Id == k))
                            {
                                //Add
                                var lev = db.Levels.Single(p => p.Id == k);
                                grade.Levels.Add(lev);

                                //
                                await db.SaveChangesAsync();
                            }
                        }
                        //
                        if(toRemove.Count > 0)
                        {
                            foreach(var h in toRemove)
                            {
                                var j = grade.Levels.SingleOrDefault(o => o.Id == h);
                                if(j != null)
                                {
                                    grade.Levels.Remove(j);
                                    await db.SaveChangesAsync();
                                }
                            }
                            //
                            ViewBag.Message = "Removed " + toRemove.Count + " Levels form grading '" + grade.Label + "' in school '"+subject.School.CountrySchool.Name+"'";
                        }
                        //
                        await db.SaveChangesAsync();
                    }
                    else if(!subject.Grades.Any(l => l.Label == model.Name))
                    {
                        //New
                        //
                        var grade = new ExamGrade();
                        grade.Label = model.Name;
                        grade.MinScore = model.MinMarks;
                        grade.Remarks = model.Comments;

                        //
                        subject.Grades.Add(grade);

                        //
                        await db.SaveChangesAsync();


                        //Add levels
                        foreach (var k in SelectedLevels)
                        {
                            if (!grade.Levels.Any(p => p.Id == k))
                            {
                                //Add
                                var lev = db.Levels.Single(p => p.Id == k);
                                grade.Levels.Add(lev);

                                //
                                await db.SaveChangesAsync();
                            }
                        }
                        ViewBag.Message = "Added" + SelectedLevels.Count() + " Levels to grading '" + grade.Label + "' in school '" + subject.School.CountrySchool.Name + "'";
                    }
                    else
                    {
                        //already exists
                        ViewBag.Message = "A grade '" + model.Name + "' with cut-off marks '" + model.MinMarks + "' already exists for this subject.";
                    }
                    
                }
                catch {
                    ModelState.AddModelError("", "Add subject's exam grade failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mode"></param>
        /// <returns></returns>
        [HttpGet("addexam", Name = SetupsControllerRoute.AddExam)]
        public async Task<IActionResult> AddExam(int mode)
        {
            ViewBag.Mode = mode;
            ViewBag.Index = 3;
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/AddTerm
        [HttpPost("addexam", Name = SetupsControllerRoute.AddExam)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddExam(AddExamViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 3;
            ViewBag.Mode = 0;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {
                    //
                    var subject = db.Subjects.Include(k=>k.School).Single(o => o.Id == model.Subject);

                    //
                    var type = db.ExamTypes.Single(p => p.Id == model.Type);

                    //all subjects
                    //var a = db.Subjects.Include(l =>l.Exams).ThenInclude(x => x.Select(q => q.Type)).ToList();
                    var all = db.Subjects.Include(l => l.Exams).Where(p => p.Name == subject.Name && p.SchoolId == subject.School.Id).ToList();
                    //
                    int added = 0;
                    //add this exam for each subject
                    foreach(var s in all)
                    {
                        var subExams = db.Exams.Include(l=>l.Type).Where(l => l.SubjectId == s.Id);
                        //var sub = db.Subjects.Include(j=>j.Exams).ThenInclude(i=>i.Select(q => q.Required)).Single(k=>k.Id== s.Id);
                       if(!subExams.Any(o=> o.Type.Id == type.Id))
                        {
                            //
                            var exam = new AcademicExam();
                            exam.Required = model.Compulsory;
                            exam.Type = type;
                            //
                            exam.SubjectId = s.Id;
                            //

                            s.Exams.Add(exam);
                            await db.SaveChangesAsync();
                            //
                            added++;
                        }else
                        {
                            ViewBag.Subject = s.Exams.FirstOrDefault(o => o.SubjectId == s.Id && o.Type.Id == type.Id)?.Id;
                        }
                    }
                    //
                    ViewBag.Message = "Registered "+added+" Exams for subject subject '" + subject.Name + "'";
                }
                catch {
                    ModelState.AddModelError("", "Add subject's exam(s) failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        // POST: /Setups/AddTerm
        [HttpPost("addsubject", Name = SetupsControllerRoute.AddSubject)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddSubject(AddSubjectViewModel model, string returnUrl = null, params int[] SelectedExams)
        {
            ViewData["ReturnUrl"] = returnUrl;
            ViewBag.Index = 2;
            if (ModelState.IsValid)
            {
                //get user name
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                try
                {
                    //
                    var school = db.Schools.Include(p=>p.Subjects).Include(l=>l.Levels).Include(k=>k.CountrySchool).Single(p => p.Id == appCache.SchoolId);

                    AcademicSubject subject = new AcademicSubject();
                    subject.Name = model.Name;
                    //
                    if (school.Subjects.Any(h => h.Name.Equals(model.Name)))
                    {
                        //All subjects..
                        var subs = school.Subjects.Where(l => l.Name == model.Name).ToList();//

                        //
                        var toRemove = new List<int>();
                        foreach (var s in subs)
                        {
                            //Get Sub
                            var sub = db.Subjects.Include(l => l.Level).Single(p => p.Id == s.Id);
                            //Get Level
                            var lev = school.Levels.Single(p => p.Id == sub.Level.Id);
                            //
                            if (!model.Levels.Any(l => l.Equals(sub.Level.Id)))
                            {
                                //ID not in new list..
                                //Has been remove from system
                                toRemove.Add(sub.Id);
                            }
                            else
                            {
                                //Was retained
                                //remove from new levels
                                model.Levels = model.Levels.Except(new int[] { sub.Level.Id }).ToArray();
                            }

                            //UPDATED EXAMS
                            //Check Added Exams
                            foreach (var m in SelectedExams)
                            {
                                if (!sub.Exams.Any(k => k.Id == m))
                                {
                                    //Updated
                                    //
                                    var e = db.Exams.Single(p => p.Id == m);
                                    sub.Exams.Add(e);

                                    await db.SaveChangesAsync();
                                }
                            }

                            //
                            //Check Removed Exams
                            var toRemove1 = new List<int>();
                            foreach (var m in sub.Exams)
                            {
                                if (!SelectedExams.Any(k => k == m.Id))
                                {
                                    //Removed
                                    toRemove1.Add(m.Id);
                                }
                            }

                            if (toRemove1.Count > 0)
                            {
                                //
                                foreach (var key in toRemove1)
                                {
                                    //
                                    var e = db.Exams.Single(p => p.Id == key);
                                    //
                                    sub.Exams.Remove(e);

                                    await db.SaveChangesAsync();
                                }

                                ViewBag.Message = toRemove.Count + " Exams have been removed from subject '" + subject.Name + "'";
                            }
                        }

                        //CONTINUE
                        //
                        if (toRemove.Count > 0)
                        {
                            foreach (var k in toRemove)
                            {
                                //remove this subject..
                                var sub = db.Subjects.Single(l => l.Id == k);
                                db.Subjects.Remove(sub);
                                //
                                await db.SaveChangesAsync();
                            }
                            ViewBag.Message = toRemove.Count + " Levels have been removed from subject '" + subject.Name + "'\n "+model.Levels.Count()+" have been added to subjects";
                        }
                        //Add subject with this new levels
                        foreach (var j in model.Levels)
                        {
                            //
                            subject.Name = model.Name;

                            var lev = school.Levels.Single(p => p.Id == j);
                            subject.Level = lev;
                            subject.Compulsory = model.Compulsory;
                            //update
                            if (SelectedExams.Count() > 0)
                            {
                                foreach (var i in SelectedExams)
                                {
                                    if (!subject.Exams.Any(p => p.Id == i))
                                    {
                                        var e = db.Exams.Single(p => p.Id == i);
                                        //
                                        subject.Exams.Add(e);
                                    }
                                }
                            }
                            //
                            subject.SchoolId = school.Id;
                            school.Subjects.Add(subject);
                            //commit
                            await db.SaveChangesAsync();
                        }

                    }
                    else
                    {
                        //Add subject with this new levels
                        foreach (var j in model.Levels)
                        {
                            //

                            var lev = school.Levels.Single(p => p.Id == j);
                            subject.Level = lev;
                            subject.Compulsory = model.Compulsory;
                            //update
                            if (SelectedExams.Count() > 0)
                            {
                                foreach (var i in SelectedExams)
                                {
                                    //exam
                                    if (!subject.Exams.Any(p => p.Id == i))
                                    {
                                        var e = db.Exams.Single(p => p.Id == i);
                                        //
                                        subject.Exams.Add(e);
                                    }
                                }
                            }
                            subject.SchoolId = school.Id;
                            school.Subjects.Add(subject);
                            //Commit
                            await db.SaveChangesAsync();
                        }
                        //
                        ViewBag.Message = model.Levels + " Subjects have been registered to school '" + school.CountrySchool.Name + "'";
                    }
                }
                catch {
                    ModelState.AddModelError("", "Add subject has failed.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Tution);
        }
        //
        // GET: /Setups/Tution/
        //[HttpPost("addtimetable", Name = SetupsControllerRoute.AddTimetable)]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> AddTimetable(AddTimeTableViewModel model,IFormFile docs, string returnUrl = null)
        //{
        //    ViewBag.Index = 5;
        //    //
        //    ViewData["ReturnUrl"] = returnUrl;

        //    if (ModelState.IsValid)
        //    {
        //        if(docs==null && (docs = model.Doc)==null)
        //        {
        //            ModelState.AddModelError("","A timetable document is required.");
        //            return View(SetupsControllerAction.Tution);
        //        }
        //        //var user = UserM
        //        var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
        //        var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
        //        try
        //        {
        //            //
        //            var term = db.Terms.Include(k => k.Year).ThenInclude(l => l.School).Single(k => k.Id == model.TermId);

        //            ///UPLOAD
        //            string filename = ContentDispositionHeaderValue.Parse(docs.ContentDisposition).FileName.Trim('"');
        //            if (filename.Contains("\\"))
        //                filename = filename.Substring(filename.LastIndexOf("\\") + 1);
        //            //Validate Extention
        //            var h = filename.Substring(filename.LastIndexOf('.'), filename.Length - filename.LastIndexOf('.'));

        //            if(!(filename.EndsWith(".png")|| filename.EndsWith(".jpg") || filename.EndsWith(".pdf") || filename.EndsWith(".jpeg")))
        //            {
        //                //
        //                ModelState.AddModelError("", "Invalid document uploaded. Valid files are 'jpg','png','jpeg' or 'pdf'");
        //                return View(SetupsControllerAction.Tution);
        //            }
        //            //filename = this.EnsureCorrectFilename(filename);
        //            var path = Path.Combine(hostingEnvironment.WebRootPath, "uploads");
        //            path = Path.Combine(path, "schools");
        //            path = Path.Combine(path, term.Year.School.Id + "");
        //            path = Path.Combine(path, "timetables");
        //            path = Path.Combine(path , term.Name.ToLower());

        //            if (!Directory.Exists(path))
        //                Directory.CreateDirectory(path);
        //            //
        //            //byte[] buffer = new byte[16 * 1024];

        //            //using (FileStream output = System.IO.File.Create(path))
        //            //{
        //            //    using (Stream input = docs.OpenReadStream())
        //            //    {
        //            //        long totalReadBytes = 0;
        //            //        int readBytes;

        //            //        while ((readBytes = input.Read(buffer, 0, buffer.Length)) > 0)
        //            //        {
        //            //            await output.WriteAsync(buffer, 0, readBytes);
        //            //            totalReadBytes += readBytes;
        //            //            //Startup.Progress = (int)((float)totalReadBytes / (float)totalBytes * 100.0);
        //            //            await Task.Delay(10); // It is only to make the process slower
        //            //        }
        //            //    }
        //            //}
        //            var tt = new ClassTimeTable();
        //            //
        //            if (docs.Length > 0)
        //            {
        //                //File Storage
        //                using (var fileStream = new FileStream(Path.Combine(path, docs.FileName), FileMode.Create))
        //                {
        //                    await docs.CopyToAsync(fileStream);
        //                }
        //                //
        //                //Binary Storage
        //                using (var memoryStream = new MemoryStream())
        //                {
        //                    await docs.CopyToAsync(memoryStream);
        //                    tt.Value = memoryStream.ToArray();
        //                }
        //            }
        //            tt.TermId = model.TermId;
        //            tt.ContentType = docs.ContentType;
        //            tt.Path = path;
        //            //
        //            term.TimeTables.Add(tt);
        //            //
        //            await db.SaveChangesAsync();
        //            //
        //            ViewBag.Message = "TimeTable " + docs.FileName + " was succesifully added to term '" + term.Name + "'.";
        //        }
        //        catch
        //        {
        //            //
        //            ModelState.AddModelError("", "Add timetable to failed.");
        //        }
        //    }

        //    //
        //    return View(SetupsControllerAction.Tution);
        //}
        // GET: /Setups/Tution/
        [HttpPost("addtimetable", Name = SetupsControllerRoute.AddTimetable)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddTimeTable(AddTimeTableViewModel model, IFormFile file,string returnUrl = null)
        {
            ViewBag.Index = 4;
            ViewBag.Mode = 2;
            //
            ViewData["ReturnUrl"] = returnUrl;
            //
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
            //
            if (model.TermId>0 && model.SubjectId>0)
            {
                ViewBag.TermId = model.TermId;
                ViewBag.SubejctId = model.SubjectId;
                //var user = UserM
                try
                {
                    ClassTimeTable table = null;
                    SubjectLesson lesson = null;
                    //
                    var term = db.Terms.Include(k=>k.TimeTables).Include(k => k.Year).ThenInclude(l => l.School).Single(k => k.Id == model.TermId);
                    var subject = db.Subjects.Single(k => k.Id == model.SubjectId);
                    if(term.TimeTables.Count > 0)
                    {
                        foreach (var t in term.TimeTables)
                        {
                            var h = db.TimeTables.Include(k => k.Lessons).ThenInclude(l => l.Select(Json => Json.Subject)).Single(k => k.Id == t.Id);
                            if (h.Lessons.Any(d => d.Subject.Id == subject.Id))
                            {
                                //
                                lesson = h.Lessons.Single(o => o.Subject.Id == subject.Id);
                                break;
                            }
                        }
                    }else
                    {
                        //New Timetable
                        table = new ClassTimeTable();
                        table.TermId = model.TermId;
                        //

                        //
                        term.TimeTables.Add(table);
                        //
                        await db.SaveChangesAsync();
                    }
                    //
                    if (lesson == null)
                        lesson = new SubjectLesson();//New lesson
                    lesson.Starts = model.Starts;
                    lesson.Ends = model.Ends;
                    //lesson.Description = model.LessonDescription;
                    //lesson.Priority = model.Priority;
                    //lesson.Title = model.Title;
                    lesson.TimeTableId = table.Id;
                    //
                    table.Lessons.Add(lesson);
                    await db.SaveChangesAsync();
                    //
                    //ViewBag.Message = "Lesson " + model.Title + " for subject '"+subject.Name+"' was succesifully added to term '" + term.Name + "'.";
                }
                catch
                {
                    //
                    ModelState.AddModelError("", "Add timetable to failed.");
                }
            }else if (model.TermId > 0)
            {
                ViewBag.TermId = model.TermId;
                model.Term = db.Terms.Single(l => l.Id == model.TermId);
            }

            //
            return View(SetupsControllerAction.Tution);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost("addlessons", Name = SetupsControllerRoute.AddLessons)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddLessons(AddTimeTableViewModel model, string returnUrl = null)
        {
            ViewBag.Index = 4;
            //
            ViewData["ReturnUrl"] = returnUrl;
            //
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
            //

            if (string.IsNullOrWhiteSpace(model.Lessons))
            {
                //Error
                ModelState.AddModelError("", "No lessons were found in post data.");
                return View(SetupsControllerAction.Tution);
            }
            //
            try
            {
                //"[{\"title\":\"Lecture I\",\"description\":\"Not Bad\",\"id\":\"\\\"2017-08-10T06:29:00.000Z\\\"\",\"start\":\"2017-08-10T06:29:00.000Z\",\"end\":\"2017-08-10T07:29:00.000Z\",\"allDay\":false,\"className\":[\"bg-color-orange\",\"txt-color-white\"],\"icon\":\"fa-check\",\"roomId\":2,\"termId\":1,\"subjectId\":1,\"_id\":\"\\\"2017-08-10T06:29:00.000Z\\\"\",\"_allDay\":false,\"_start\":\"2017-08-10T06:29:00.000Z\",\"_end\":\"2017-08-10T07:29:00.000Z\"}]"
                JArray lessons = JArray.Parse(model.Lessons);
                //
                if (lessons.Count == 0)
                {
                    //Error//
                    ModelState.AddModelError("", "No lessons were found in post data.");
                    return View(SetupsControllerAction.Tution);
                }
                //
                ClassTimeTable timeTable = null;
                //
                foreach (var obj in lessons)
                {
                    var parsed = JsonConvert.DeserializeObject<Lesson>(obj.ToString());
                    //
                    var term = db.Terms.Include(y=>y.Year).Single(o => o.Id == parsed.termId);
                    if(parsed.start > term.Ends){
                        //
                        ModelState.AddModelError("", "Add lesson '" + parsed.title + "' to timetable failed. Wrong Term ('"+term.Name+" -Year "+term.Year.Name+"').");
                        continue;
                    }
                    //
                    try
                    {
                        //
                        var room = db.Classrooms.Single(k => k.Id == parsed.roomId);
                        //
                        var subject = db.Subjects.Single(k => k.Id == parsed.subjectId);
                        timeTable = db.TimeTables.Include(k=>k.Lessons).SingleOrDefault(k => k.TermId == parsed.termId);
                        if (timeTable == null)
                        {
                            timeTable = new ClassTimeTable();
                            //
                            timeTable.TermId = term.Id;
                            //
                            term.TimeTables.Add(timeTable);
                            await db.SaveChangesAsync();
                        }else if(timeTable.Lessons.Count > 0)
                        {
                            if(timeTable.Lessons.Any(p=>(parsed.start.TimeOfDay >= p.Starts.TimeOfDay && parsed.start.TimeOfDay < p.Starts.TimeOfDay)))
                            {
                                foreach(var l in timeTable.Lessons.Where(p => (parsed.start.TimeOfDay >= p.Starts.TimeOfDay && parsed.start.TimeOfDay < p.Starts.TimeOfDay)).ToList())
                                {
                                    var les = db.Lessons.Include(f=>f.Classroom).Single(p => p.Id == l.Id);
                                    if(les.Classroom.Id == parsed.roomId)
                                    {
                                        //Allready occupied splot
                                        ModelState.AddModelError("", "Add lesson '" + parsed.title + "' to timetable failed. Classroom '"+room.Name+"' is occupied in the specified lesson time");
                                    }
                                }
                            }
                            //
                            if(ModelState.ErrorCount > 0)
                            {
                                //
                                return View(SetupsControllerAction.Tution);
                            }
                        }
                        //Initial lesson
                        var lesson = new SubjectLesson();
                        lesson.IsFullDay = parsed.allDay;
                        lesson.Icon = parsed.icon;
                        lesson.Starts = parsed.start;
                        lesson.Ends = parsed.end;
                        lesson.Description = parsed.description;
                        lesson.Title = parsed.title;
                        lesson.Priority = parsed.className.ToString();
                        lesson.TimeTableId = timeTable.Id;
                        lesson.Subject = subject;
                        lesson.Classroom = room;
                        //
                        timeTable.Lessons.Add(lesson);
                        await db.SaveChangesAsync();
                        //Add lesson for each week
                        for (var index = 0; index < CountDays(parsed.start.DayOfWeek, parsed.start, term.Ends); index++)
                        {
                            //
                            lesson = new SubjectLesson();
                            lesson.IsFullDay = parsed.allDay;
                            lesson.Icon = parsed.icon;
                            lesson.Starts = parsed.start.AddDays(7 * index);
                            lesson.Ends = parsed.end.AddDays(7 * index);
                            lesson.Description = parsed.description;
                            lesson.Title = parsed.title;
                            lesson.Priority = parsed.className.ToString();
                            lesson.TimeTableId = timeTable.Id;
                            lesson.Subject = subject;
                            lesson.Classroom = room;
                            //
                            timeTable.Lessons.Add(lesson);
                            await db.SaveChangesAsync();
                        }
                    }
                    catch
                    {
                        ModelState.AddModelError("", "Add lesson '"+parsed.title+"' to timetable failed.");
                    }
                }
                //
                ViewBag.TermId = timeTable.TermId;

                if (ModelState.ErrorCount > 0)
                {
                    ViewBag.Warning = "Some lessons could not be added to the timetable.";
                }else
                {
                    ViewBag.Message = "Lessons were added to timetable succesifuly.";
                }
            }
            catch
            {
                //Failed
                ModelState.AddModelError("", "Add lessons to timetable failed.");
            }
            //
            return View(SetupsControllerAction.Tution);
        }


        //
        // GET: /Setups/Staff/
        [HttpGet("staff", Name = SetupsControllerRoute.GetStaff)]
        public IActionResult Staff(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            return View(SetupsControllerAction.Staff);
        }

        //
        // POST: /Setups/Staff
        [HttpPost("addstaff", Name = SetupsControllerRoute.AddStaff)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Staff(AddStaffViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                //get user name
                //var name = model.Email;
                //var user = await _userManager.FindByEmailAsync(model.Email);

            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Staff,model);
        }
        // POST: /Setups/addStudents
        [HttpPost("addStudent", Name = SetupsControllerRoute.AddStudent)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddStudent(AddStudentViewModel model,string returnUrl = null)
        {
            ViewBag.Index = 0;
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                //get user name
                //var name = model.Email;
                //var user = await _userManager.FindByEmailAsync(model.Email);

            }

            // If we got this far, something failed, redisplay form
            return View(SetupsControllerAction.Staff, model);
        }

        [HttpPost("uploadStudents", Name = SetupsControllerRoute.UploadStudents)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UploadStudents(List<IFormFile> files, string returnUrl = null)
        {
            ViewBag.Index = 0;
            ViewData["ReturnUrl"] = returnUrl;
            if(files.Count == 0)
                files = Request.Form.Files.ToList();
            //
            if (files != null && files.Count > 0)
            {
                //Upload & import
                var uploads = new List<ExcelPackage>();
                var path = Path.Combine(Path.Combine(hostingEnvironment.WebRootPath, "uploads"), "temp");
                foreach (var file in files)
                {
                    if (file.Length > 0)
                    {
                        //
                        //if (!System.IO.Directory.Exists(j))
                        //    System.IO.Directory.CreateDirectory(j);
                        ////
                        //using (var fileStream = new FileStream(j, FileMode.Create))
                        //{
                        //    await file.CopyToAsync(fileStream);
                        //    uploads.Add(j);
                        //}
                        //using (var ms = new MemoryStream())
                        //{
                        //    file.CopyTo(ms);
                        //    //

                        //}
                        using (ExcelPackage excelPackage = new ExcelPackage(file.OpenReadStream()))
                        {
                            //work with the excel document
                            uploads.Add(excelPackage);
                        }
                    }
                }
                //
                foreach(var p in uploads)
                {
                    var g = ImportExcelFile(p);
                }
            }
            else
            {
                //
                string sWebRootFolder = hostingEnvironment.WebRootPath;
                string sFileName = @"demo.xlsx";
                string URL = string.Format("{0}://{1}/Exports/{2}", Request.Scheme, Request.Host, sFileName);

                //
                var path = Path.Combine(Path.Combine(sWebRootFolder, "exports"), sFileName);

                if (ExportToExcel(path)){
                    //
                    using (ExcelPackage excelPackage = new ExcelPackage(new FileInfo(path)))
                    {
                        //work with the excel document
                        var g = ImportExcelFile(excelPackage);
                    }
                }
                else
                {

                }
            }

            // If we got this far, something failed, redisplay form
            return new JsonResult(new { s = 0 });
        }
        //
        // GET: /Setups/Schools/
        [HttpGet("finance", Name = SetupsControllerRoute.GetFinance)]
        public IActionResult Finance(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            return View(SetupsControllerAction.Finance);
        }

        //
        // POST: /Setups/finance
        [HttpPost("finance", Name = SetupsControllerRoute.GetFinance)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Finance(FinanceViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                //get user name
                //var name = model.Email;
                //var user = await _userManager.FindByEmailAsync(model.Email);

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Setups/Ammenities/
        [HttpGet("ammenities", Name = SetupsControllerRoute.GetAmmenities)]
        public IActionResult Ammenities(string returnUrl = null)
        {
            //
            ViewData["ReturnUrl"] = returnUrl;
            return View(SetupsControllerAction.Ammenity);
        }

        //
        // POST: /Setups/Ammenities
        [HttpPost("ammenities", Name = SetupsControllerRoute.GetAmmenities)]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Ammenities(AmmenityViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                //get user name
                //var name = model.Email;
                //var user = await _userManager.FindByEmailAsync(model.Email);

            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        /// <summary>
        /// Requests to dynamic server data.
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        /// 
        [HttpGet("bundles", Name = SetupsControllerRoute.GetBundle)]
        public async Task<JsonResult> GetBundle(string q)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                var query = JsonConvert.DeserializeObject<BundleRequest>(q);
                //
                var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();
                //
                result.Append("{");
                switch (query.q)
                {
                    case "levs":
                        //
                        var levels = db.Levels.Where(p => p.SchoolId == query.sid).ToList();
                        result.Append("'s':'1','data':[");
                        if (levels.Count > 0)
                        {
                            foreach(var l in levels)
                            {
                                result.Append("{'id':'"+l.Id+"','name':'"+l.Name+"'}");
                                result.Append(",");
                            }
                            //
                            result = AppUtils.TrimBuilder(result);
                        }
                        result.Append("],'err':[]");
                        break;
                    case "streams":
                        //
                        var streams = db.Schools.Include(k=>k.Streams).Single(p => p.Id == query.sid).Streams.ToList();
                        result.Append("'s':'1','data':[");
                        if (streams.Count > 0)
                        {
                            foreach (var l in streams)
                            {
                                result.Append("{'id':'" + l.Id + "','name':'" + l.Name + "'}");
                                result.Append(",");
                            }
                            //
                            result = AppUtils.TrimBuilder(result);
                        }
                        result.Append("],'err':[]");
                        break;
                    default:
                        //
                        result.Append("'s':'0','err':['Bad query'],'data':[]");
                        break;
                }
                result.Append("}");
            }
            catch(Exception e)
            {

                return new JsonResult("{'s':'0','err':['"+e.Message+"'],data:[]}");
            }

            //
            return new JsonResult(JToken.Parse(result.ToString()));
        }
        public List<Dictionary<string,string>> ImportExcelFile(ExcelPackage package)
        {
            string sWebRootFolder = hostingEnvironment.WebRootPath;
            var parsed = new List<Dictionary<string, string>>();
            try
            {
                StringBuilder sb = new StringBuilder();
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];
                int rowCount = worksheet.Dimension.Rows;
                int ColCount = worksheet.Dimension.Columns;
                //
                var headers = new string[ColCount];
                bool bHeaderRow = true;
                //
                for (int row = 1; row <= rowCount; row++)
                {
                    for (int col = 1; col <= ColCount; col++)
                    {
                        if (bHeaderRow)
                        {
                            sb.Append(worksheet.Cells[row, col].Value.ToString() + "\t");
                            //
                            headers[col] = worksheet.Cells[row, col].Value.ToString();
                        }
                        else
                        {
                            sb.Append(worksheet.Cells[row, col].Value.ToString() + "\t");

                            var j = new Dictionary<string, string>();
                            j.Add(headers[col], worksheet.Cells[row, col].Value.ToString());
                            //
                            parsed.Add(j);
                        }
                    }
                    sb.Append(Environment.NewLine);
                    if (row > 1)
                    {
                        bHeaderRow = false;
                    }
                }
                System.Diagnostics.Debug.WriteLine(sb.ToString());
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Failed" + ex.Message);
            }
            //

            return parsed;
        }

        public bool ExportToExcel(string filePath)
        {

            try
            {
                FileInfo file = new FileInfo(filePath);
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(filePath);
                }else
                {
                    //
                    if (!file.Directory.Exists)
                        file.Directory.Create();
                }
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Employee");

                    //First add the headers
                    worksheet.Cells[1, 1].Value = "ID";
                    worksheet.Cells[1, 2].Value = "Name";
                    worksheet.Cells[1, 3].Value = "Gender";
                    worksheet.Cells[1, 4].Value = "Salary (in $)";

                    //Add values
                    worksheet.Cells["A2"].Value = 1000;
                    worksheet.Cells["B2"].Value = "Jon";
                    worksheet.Cells["C2"].Value = "M";
                    worksheet.Cells["D2"].Value = 5000;

                    worksheet.Cells["A3"].Value = 1001;
                    worksheet.Cells["B3"].Value = "Graham";
                    worksheet.Cells["C3"].Value = "M";
                    worksheet.Cells["D3"].Value = 10000;

                    worksheet.Cells["A4"].Value = 1002;
                    worksheet.Cells["B4"].Value = "Jenny";
                    worksheet.Cells["C4"].Value = "F";
                    worksheet.Cells["D4"].Value = 5000;

                    //Format Headers
                    using (var cells = worksheet.Cells[1, 1, 1, 4])
                    {
                        cells.Style.Font.Bold = true;
                        cells.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        cells.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    }

                    package.Save(); //Save the workbook.
                }
            }
            catch
            {
                return false;
            }
            return true;
        }
        #region
        static int CountDays(DayOfWeek day, DateTime start, DateTime end)
        {
            TimeSpan ts = end - start;                       // Total duration
            int count = (int)Math.Floor(ts.TotalDays / 7);   // Number of whole weeks
            int remainder = (int)(ts.TotalDays % 7);         // Number of remaining days
            int sinceLastDay = (int)(end.DayOfWeek - day);   // Number of days since last [day]
            if (sinceLastDay < 0) sinceLastDay += 7;         // Adjust for negative days since last [day]

            // If the days in excess of an even week are greater than or equal to the number days since the last [day], then count this one, too.
            if (remainder >= sinceLastDay) count++;

            return count;
        }
        #endregion
    }

    #region
    public class Lesson
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public bool allDay { get; set; }
        public List<string> className { get; set; }
        public string icon { get; set; }
        public int termId { get; set; }
        public int subjectId { get; set; }
        public int roomId { get; set; }
        public DateTime _start { get; set; }
        public DateTime _end { get; set; }
        public bool _allDay { get; set; }
        public string _id { get; set; }
    }

    public class BundleRequest
    {
        public string q { get; set; }
        public int sid { get; set; }
        public int lid { get; set; }
        public int cid { get; set; }
    }
    #endregion
}
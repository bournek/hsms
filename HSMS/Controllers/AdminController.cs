﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Newtonsoft.Json;
using com.bkss.Services;
using Microsoft.AspNetCore.Authorization;
using com.bkss.Settings;
using Microsoft.AspNetCore.Mvc;
using com.bkss.Constants;
using Microsoft.AspNetCore.Identity;
using com.bkss.IdentityModels;
using com.bkss.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace com.bkss.Controllers
{

    [Authorize(Roles = AppRoles.AppAdminRole)]
    public class AdminController : Controller
    {
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public AdminController() { 
            //
        }
        // GET: Admin
        [Route("admin", Name = AdminControllerRoute.GetIndex)]
        public ActionResult Index()
        {
           
            return View();
        }
        [Route("roles", Name = AdminControllerRoute.GetRoles)]
        public ActionResult Roles()
        {
            //return View(RoleManager.Roles);
            return View(_roleManager.Roles);
        }
        public async Task<ActionResult> RoleDetails()
        {
            return View();
        }
        public ActionResult CreateRole()
        {
            return View();
        }

        //
        // POST: /Roles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateRole(RoleViewModel roleViewModel)
        {
            if (ModelState.IsValid)
            {
                // Use ApplicationRole, not IdentityRole:
                var role = new ApplicationRole(roleViewModel.Name);
                var roleresult = await _roleManager.CreateAsync(role);
                if (!roleresult.Succeeded)
                {
                    ModelState.AddModelError("", roleresult.Errors.First().Description);
                    return View();
                }
                return RedirectToAction("Index");
            }
            return View();
        }
        public async Task<ActionResult> EditRole(int id)
        {
           
            if (id > 0)
            {
                var role = await _roleManager.FindByIdAsync(id+"");
                if (role == null)
                {
                    return NotFound();
                }
                RoleViewModel roleModel = new RoleViewModel { Id = role.Id, Name = role.Name };
                return View(roleModel);
            }
            return StatusCode((int)HttpStatusCode.BadRequest);
        }

        //
        // POST: /Roles/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditRole(RoleViewModel roleModel)
        {
            if (ModelState.IsValid)
            {
                var role = await _roleManager.FindByIdAsync(roleModel.Id+"");
                role.Name = roleModel.Name;
                await _roleManager.UpdateAsync(role);
                return RedirectToAction("Index");
            }
            return View();
        }

        //
        // GET: /Roles/Delete/5
        public async Task<ActionResult> DeleteRole(int id)
        {
            
            if (id > 0)
            {
                var role = await _roleManager.FindByIdAsync(id+"");
                if (role == null)
                {
                    return NotFound();
                }
                return View(role);
            }
            return new StatusCodeResult((int)HttpStatusCode.BadRequest);
        }

        //
        // POST: /Roles/Delete/5
        [HttpPost, ActionName("DeleteRole")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteRoleConfirmed(int id)
        {

            if (ModelState.IsValid)
            {
                if (id > 0)
                {
                    var role = await _roleManager.FindByIdAsync(id+"");
                    if (role == null)
                    {
                        return NotFound();
                    }
                    IdentityResult result = await _roleManager.DeleteAsync(role);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First().Description);
                        return View();
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);
                }
            }
            return View();
        }
        [Route("users", Name = AdminControllerRoute.GetUsers)]
        public ActionResult Users()
        {
            UsersViewModel model = new UsersViewModel();

            var list = _userManager.Users.ToList();
            List<int> users = new List<int>();
            foreach (var user in list)
                users.Add(user.Id);
            System.Diagnostics.Debug.WriteLine("Total Users = " + users.Count);
            
            model.Users = users;
            //
            model.Roles = _roleManager.Roles.ToList();
            //
            return View(model);
        }
        public async Task<ActionResult> UserDetails(int id)
        {
           
            //
            if (id > 0)
            {
                // Process normally:
                var user = await _userManager.FindByIdAsync(id+"");
                ViewBag.RoleNames = await _userManager.GetRolesAsync(user);
                return View(user);
            }
            // Return Error:
            return new StatusCodeResult((int)HttpStatusCode.BadRequest);
        }
        public async Task<ActionResult> DeleteUser(int id)
        {
            
            if (id > 0)
            {
                var user = await _userManager.FindByIdAsync(id+"");
                if (user == null)
                {
                    return NotFound();
                }
                return View(user);
            }
            return new StatusCodeResult((int)HttpStatusCode.BadRequest);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUserConfirmed(int uid)
        {
            if (ModelState.IsValid)
            {
                if (uid > 0)
                {
                    var user = await _userManager.FindByIdAsync(uid+"");
                    if (user == null)
                    {
                        return NotFound();
                    }
                    var result = await _userManager.DeleteAsync(user);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First().Description);
                        return View();
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return new StatusCodeResult((int)HttpStatusCode.BadRequest);
                }
            }
            return View();
        }
        public ActionResult UsersRoles()
        {
            List<ApplicationUser> users = new List<ApplicationUser>();
            foreach(var user in _userManager.Users.Where(k => k.Roles.Count > 0).ToList())
            {
                users.Add(user);
            }
            return View(users);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ActionName("UserRoles")]
        public async Task<ActionResult> ManageUserRoles(int id)
        {
           
            if (id > 0)
            {
                EditUserViewModel model = new EditUserViewModel();
                var user = await _userManager.FindByIdAsync(id+"");
                if (user == null)
                {
                    return NotFound();
                }

                var userRoles = await _userManager.GetRolesAsync(user);
                return View(new EditUserViewModel()
                {
                    Id = user.Id,
                    Email = user.Email,
                    Roles = _roleManager.Roles.ToList().Select(x => new SelectListItem()
                    {
                        Selected = userRoles.Contains(x.Name),
                        Text = x.Name,
                        Value = x.Name
                    })
                });
            }
            return new StatusCodeResult((int)HttpStatusCode.BadRequest);
        }
        //
        [HttpPost, ActionName("UserRoles")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ManageUserRoles(EditUserViewModel model, params string[] selectedRoles)
        {
            System.Diagnostics.Debug.WriteLine("Admin/ManageUserRoles...POST");

            try
            {
                System.Diagnostics.Debug.WriteLine("Model is valid.");
                var user = await _userManager.FindByIdAsync(model.Id+"");
                if (user == null)
                {
                    return NotFound();
                }
                var userRoles = await _userManager.GetRolesAsync(user);
                selectedRoles = selectedRoles ?? new string[] { };
                System.Diagnostics.Debug.WriteLine("Add user to roles...");
                var result = await _userManager.AddToRolesAsync(user, selectedRoles.Except(userRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    System.Diagnostics.Debug.WriteLine("Add user to roles failed..");
                    ModelState.AddModelError("", result.Errors.First().Description);
                    return View(new EditUserViewModel()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Roles = _roleManager.Roles.ToList().Select(x => new SelectListItem()
                        {
                            Selected = userRoles.Contains(x.Name),
                            Text = x.Name,
                            Value = "" + x.Id
                        })
                    });
                }
                System.Diagnostics.Debug.WriteLine("Remove user from roles..");
                result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRoles).ToArray<string>());

                if (!result.Succeeded)
                {
                    System.Diagnostics.Debug.WriteLine("remove user from roles failed..");
                    ModelState.AddModelError("", result.Errors.First().Description);
                    return View(new EditUserViewModel()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        Roles = _roleManager.Roles.ToList().Select(x => new SelectListItem()
                        {
                            Selected = userRoles.Contains(x.Name),
                            Text = x.Name,
                            Value = x.Name
                        })
                    });
                }
                System.Diagnostics.Debug.WriteLine("User updating was successiful.");
                return RedirectToAction("Index");
            }catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Model is not valid");
                //HwrManager.Print
                foreach (var d in ModelState.Values)
                {
                    foreach (ModelError e in d.Errors)
                    {
                        System.Diagnostics.Debug.WriteLine("Entry : " + d.RawValue + ", Errors : " + e.ErrorMessage);
                    }
                }
                ModelState.AddModelError("", "Update user roles failed.");
                return View(model);
            }
           
        }

    }
}

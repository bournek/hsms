﻿namespace com.bkss.Controllers
{
    using Boilerplate.AspNetCore;
    using Microsoft.AspNetCore.Mvc;
    using com.bkss.Constants;

    /// <summary>
    /// Provides methods that respond to HTTP requests with HTTP errors.
    /// </summary>
    public sealed class ProfileController : Controller
    {
        #region Public Methods
        [HttpGet("profile", Name = ProfileControllerRoute.GetProfile)]
        public IActionResult Index()
        {
            //
            return this.View(ProfileControllerAction.Inbox);
        }
        [HttpGet("inbox", Name = ProfileControllerRoute.GetInbox)]
        public IActionResult Mail()
        {
            //
            return this.View(ProfileControllerAction.Inbox);
        }
        [HttpGet("calendar", Name = ProfileControllerRoute.GetCalendar)]
        public IActionResult Events()
        {
            //
            return this.View(ProfileControllerAction.Calendar);
        }
        #endregion
    }
}
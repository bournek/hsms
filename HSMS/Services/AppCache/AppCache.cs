﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using com.bkss.IdentityModels;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace com.bkss.Services
{
    public class AppCache : IAppCacheService
    {
        private readonly IServiceProvider serviceProvider;

        private static int _schoolId;

        public AppCache(IServiceProvider provider)
        {
            serviceProvider = provider;
        }
        public School GetSchool()
        {
            if(_schoolId == 0)
                throw new NotImplementedException();
            //
            var scoped = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            var db = scoped.ServiceProvider.GetService<ApplicationDbContext>();

            return db.Schools.Include(j=>j.CountrySchool).Single(p => p.Id == _schoolId);
        }

        public int SchoolId { get { return _schoolId; }set { _schoolId = value; } }
    }
}

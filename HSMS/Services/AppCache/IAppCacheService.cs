﻿using com.bkss.IdentityModels;

namespace com.bkss.Services
{
    public interface IAppCacheService
    {
        School GetSchool();
    }
}

﻿//
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.bkss.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using com.bkss.Services;
using Microsoft.Extensions.Logging;
using com.bkss.Middlewares.Utils;
using com.bkss.IdentityModels;

namespace com.bkss.Handlers
{
    public partial class RequestManager
    {
        private static HttpContext mContext;
        private static IServiceScope mServiceScope;
        private static ILogger _logger;
        public RequestManager(IServiceProvider provider)
        {
            mServiceScope = provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            System.Diagnostics.Debug.WriteLine("Get Services..");
            System.Diagnostics.Debug.WriteLine("Get Logger...");
            //
            ILoggerFactory logger = provider.GetRequiredService<ILoggerFactory>();
            //
            _logger = logger.CreateLogger<RequestManager>();
        }
        public StringBuilder handle(HttpContext context, IQueryCollection collection)
        {
            mContext = context;
            StringBuilder builder = new StringBuilder();
            //
            //System.Diagnostics.Debug.WriteLine("*************************^^^^^^^^^^^^^^^^^^**************************");
            _logger.LogInformation(2, "Process Request... : Keys = "+ collection.Keys.Count);
            builder.Append("[");
            //parse reaponse
            foreach (string key in collection.Keys)
            {
                System.Diagnostics.Debug.WriteLine("*************************^^^^^^^^^^^^^^^^^^**************************");
                System.Diagnostics.Debug.WriteLine("Key :> " + (key ?? "NOT SET") + ", Value => " + collection.FirstOrDefault(k => k.Key == key).Value);
                System.Diagnostics.Debug.WriteLine("*************************VVVVVVVVVVVVVVVVVV**************************");
                if (key != null)
                {

                    if (key.Equals("q"))
                    {
                        switch (collection.First(k => k.Key == key).Value)
                        {
                            case "usr":
                                //
                                System.Diagnostics.Debug.WriteLine("Fetch user...");
                                builder = FindUser(builder, context.Request.Form);
                                break;
                            case "add":
                                //
                                System.Diagnostics.Debug.WriteLine("Add...");
                                break;
                            case "update":
                                //
                                System.Diagnostics.Debug.WriteLine("Update...");
                                if (collection.Keys.Contains("typ"))
                                {
                                    string typ = collection.FirstOrDefault(l => l.Key == "typ").Value;
                                    if (typ != null && typ == "loc")
                                    {
                                        System.Diagnostics.Debug.WriteLine("Update Location...");
                                        builder = UpdateLocation(builder, context.Request.Form);
                                    }else if (typ != null && typ == "prof")
                                    {
                                        System.Diagnostics.Debug.WriteLine("Update profile...");
                                        builder = UpdateProfile(builder, context.Request.Form);
                                    }

                                }
                                break;
                            case "start":
                                System.Diagnostics.Debug.WriteLine("Start Session...");
                                builder = StartSession(builder, context.Request.Form);
                                break;
                            case "end":
                                System.Diagnostics.Debug.WriteLine("End session...");
                                builder = EndSession(builder, context.Request.Form);
                                break;
                            case "loc":
                                System.Diagnostics.Debug.WriteLine("Check email...");
                                builder = GetLocation(builder, context.Request.Form);
                                break;
                            case "reg":
                                //
                                System.Diagnostics.Debug.WriteLine("Join User.....");
                                //collection.Remove(key);
                                var nvc = context.Request.Form;
                                int y = RegisterClient(builder, nvc);
                                if (y > 0)
                                    builder.Append("{'action':'reg','state':'1','"+ProfileKeys.JSON_KEY_ID+"':'"+y+"'}");
                                break;
                            case "del":
                                
                                break;
                            case "sms":
                                //
                                //Messaging.SendTestSMS("Test one two one two...");
                                break;
                            case "m"://Mark as read
                                
                                break;
                            case "u":

                                
                                break;
                            case "r":

                                
                                break;
                            case "rv":

                                
                                break;
                            case "sol":
                                
                                break;
                            case "acc":
                                //
                                System.Diagnostics.Debug.WriteLine("Check email...");
                                
                                break;
                            case "msg":
                                //System.Diagnostics.Debug.WriteLine("Get Messages..");
                                
                                break;
                            case "cv":
                                //Conversion Value
                                break;
                            default:
                                break;
                        }
                        break;

                    }
                    else if (key.Equals("typ"))
                    {
                         if (collection.First(k => k.Key == key).Value.Equals("upload"))
                        {
                            if (collection.First(k => k.Key == key).Value.Equals("attach"))
                            {
                                //System.Diagnostics.Debug.WriteLine("Attachment....");
                            }
                        }
                        else if (collection.First(k => k.Key == key).Value.Equals("rating"))
                        {
                            var u = collection.ContainsKey("u");
                            builder.Append("{\"s\":\"0\"}");                            
                        }
                        else
                        {

                        }
                        break;
                    }
                    else if (key.Equals("opt"))
                    {
                        switch (collection.First(k => k.Key == key).Value)
                        {
                            case "cv":
                                //NOLONGER NECESSARY
                                try
                                {
                                    var t = int.Parse(collection.First(k => k.Key == "val").Value);
                                    //HwrManager.GetInstance().EuroConversionFactor = t;
                                    builder.Append("{\"s\":\"0\"}");
                                }
                                catch (Exception e)
                                {
                                    System.Diagnostics.Debug.WriteLine("Set Conversion Rate Failed. Error = " + e.Message);
                                    builder.Append("{\"s\":\"0\"}");
                                }
                                break;
                            default:
                                //
                                break;
                        }
                    }
                    else
                    {
                        //
                        //System.Diagnostics.Debug.WriteLine("Invalid Request from " + mContext.Request.UserHostAddress);

                    }
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine("Parse Chat.... Request = "+mContext.Request.QueryString);
                    if (collection.First(k => k.Key == key).Value == "chat" && mContext.User.Identity.IsAuthenticated)
                    {
                        //System.Diagnostics.Debug.WriteLine("Q = " + collection.First(k => k.Key == "q").Value);
                        
                        break;
                    }
                    //Key is null
                }
            }
            builder.Append("]");
            return builder;
        }
        #region private implementations
        private int RegisterClient(StringBuilder result,IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("Register mobile client...");

            foreach(var h in values)
            {
                System.Diagnostics.Debug.WriteLine("Entry : " + h.Key+", Value = " + h.Value);
            }
            //
            if (values.Keys.Contains(ProfileKeys.JSON_KEY_DEVICE_ID))
            {
                string key = values.Single(p => p.Key == ProfileKeys.JSON_KEY_DEVICE_ID).Value;

                var db = mServiceScope.ServiceProvider.GetService<ApplicationDbContext>();
                
            }
            //
            return 0;
        }
        private StringBuilder FindUser(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("Find mobile client...");
            if (!values.Keys.Contains(ProfileKeys.JSON_KEY_ID))
            {
                //
                result.Append("{'action':'reg','state':'0'}");
            }

            var db = mServiceScope.ServiceProvider.GetService<ApplicationDbContext>();

            return result;
        }
        private StringBuilder StartSession(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("Start TriGlobe Session...");

            return result;
        }
        private StringBuilder EndSession(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("End TriGlobe session...");

            return result;
        }
        private StringBuilder GetLocation(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("Get mobile client location...");

            return result;
        }
        private StringBuilder UpdateLocation(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("update client location...");

            return result;
        }
        private StringBuilder UpdateProfile(StringBuilder result, IFormCollection values)
        {
            //
            System.Diagnostics.Debug.WriteLine("update mobile client profile...");

            return result;
        }
        #endregion
    }
}
﻿
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace com.bkss.Handlers
{
    public class AsyncHttpHandler
    {
       
        private AsyncRequestManager asynch;
        
        IServiceProvider mProvider;
        public AsyncHttpHandler(IServiceProvider provider)
        {
            mProvider = provider;
        }
        public IAsyncResult BeginProcessRequest(HttpContext context, AsyncCallback cb, Object extraData)
        {          
            
            //context.Response.Write("<p>Begin IsThreadPoolThread is " + Thread.CurrentThread.IsThreadPoolThread + "</p>\r\n");
            asynch = new AsyncRequestManager(mProvider,cb, context, extraData);
            asynch.StartAsyncWork();
            return asynch;
        }

        public void EndProcessRequest(IAsyncResult result)
        {
        }

        public void ProcessRequest(HttpContext context)
        {
            System.Diagnostics.Debug.WriteLine("AsyncHttpHandler> : Process Request...");
            throw new InvalidOperationException();
        }
    }
    /// <summary>
    /// This Method is called after the request has been processed.
    /// </summary>
    class AsyncRequestManager : IAsyncResult
    {
        private static string TAG = "AsyncWorker > ";
        private bool _completed;
        private Object _data;
        private AsyncCallback _callback;
        private HttpContext _context;
        private static AsyncRequestManager _Instance;
        private TaskCompletionSource<bool> _uploadFlag = new TaskCompletionSource<bool>();
        public static AsyncRequestManager GetInstance { get { return _Instance; } }
        public bool IsComplete { get { return _completed; } }
        public TaskCompletionSource<bool> ProcessAwaiter { get { return _uploadFlag; } }
        bool IAsyncResult.IsCompleted { get { return _completed; } }
        WaitHandle IAsyncResult.AsyncWaitHandle { get { return null; } }
        Object IAsyncResult.AsyncState { get { return _data; } }
        bool IAsyncResult.CompletedSynchronously { get { return false; } }

        private IServiceProvider _provider;
        private MemoryStream mStream = null;

        public AsyncRequestManager(IServiceProvider provider,AsyncCallback callback, HttpContext context, Object data)
        {
            _callback = callback;
            _context = context;
            _data = data;
            _completed = false;
            _Instance = this;
            _provider = provider;
            if (data is MemoryStream)
            {
                mStream = (MemoryStream)data;
            }
        }

        public void StartAsyncWork()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(StartAsyncTask), null);
        }

        private async void StartAsyncTask(Object workItemState)
        {

            //get query param as name-value pairs
            var opts = _context.Request.Query;
            System.Diagnostics.Debug.WriteLine("Quesry String = " + _context.Request.QueryString);
            if (_context.User.Identity.IsAuthenticated)
            {
                /*if (_context.Request.Files.Count > 0 )
                {
                    try
                    {
                        System.Diagnostics.Debug.WriteLine("Request has attached files. Parsing...");
                        System.Diagnostics.Debug.WriteLine("Async Handler = Upload File ... Count = " + _context.Request.Files.Count);
                        new FileManager().ProcessRequest(_context);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine("File upload failed.");
                        _context.Response.ContentType = "text/html";
                        _context.Response.Write("<p>Upload Failed, Error: " + e.Message + "</p>");
                    }
                    
                }
                else*/
                if (opts.Any(p=>p.Key=="f"))
                {
                    System.Diagnostics.Debug.WriteLine("Manage File = Mask = " + opts.First(k => k.Key == "f").Value + ", Opt = " + opts.First(k => k.Key == "opt").Value);
                    if (opts.Any(k=>k.Key=="opt") && opts.First(k=>k.Key=="opt").Value.Equals("del"))
                    {
                        //new FileManager().DeleteFiles(_context);
                    }
                    else if (opts.First(k => k.Key == "opt").Value == "dl")
                    {
                        //Download
                        //new FileManager().ProcessRequest(_context);

                    }

                }
                else
                {
                    // new FileManager().ProcessRequest(_context);
                    try
                    {
                        //process and write output as text
                        string response = new RequestManager(_provider).handle( _context, opts).ToString();
                        System.Diagnostics.Debug.WriteLine(TAG + "Response : " + response);
                        byte[] data = Encoding.UTF8.GetBytes(JToken.Parse(response).ToString());
                        _context.Response.ContentType = "application/json";
                        await _context.Response.Body.WriteAsync(data, 0, data.Length);
                        //mStream.Seek(0, SeekOrigin.Begin);
                        //using (var streamReader = new StreamReader(mStream))
                        //{
                        //    var responseBody = await streamReader.ReadToEndAsync();
                        //    //write response here
                        //    responseBody = response;
                        //    //
                        //    using (var amendedBody = new MemoryStream())
                        //    using (var streamWriter = new StreamWriter(amendedBody))
                        //    {
                        //        streamWriter.Write(responseBody);
                        //        amendedBody.Seek(0, SeekOrigin.Begin);
                        //        await amendedBody.CopyToAsync(_context.Request.Body);
                        //    }
                        //}
                    }
                    catch(Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(TAG + " Parse req. failed.\n"+e.StackTrace);
                        _context.Response.StatusCode = 500;
                    }
                }
            }
            else
            {
                try
                    {
                    //process and write output as text
                    string response = new RequestManager(_provider).handle(_context, opts).ToString();
                    System.Diagnostics.Debug.WriteLine(TAG + "Response : " + response);
                    mStream.Seek(0, SeekOrigin.Begin);
                    using (var streamReader = new StreamReader(mStream))
                    {
                        var responseBody = await streamReader.ReadToEndAsync();
                        //write response here
                        responseBody = response;
                        //
                        using (var amendedBody = new MemoryStream())
                        using (var streamWriter = new StreamWriter(amendedBody))
                        {
                            streamWriter.Write(responseBody);
                            amendedBody.Seek(0, SeekOrigin.Begin);
                            await amendedBody.CopyToAsync(_context.Request.Body);
                        }
                    }
                    //
                    //using (var writeResponseStream = new StreamWriter(_context.Response.Body))
                    //{
                        // write html to response body
                     //   Task.Run(async () => await writeResponseStream.WriteAsync(response));
                    //}
                }
                catch(Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(TAG + " Parse req. failed.\n"+e.StackTrace);
                    _context.Response.StatusCode = 500;
                }
            }

            //System.Diagnostics.Debug.WriteLine("AsyncHttpHandler.. Exit");
            //set flag
            _uploadFlag.SetResult(true);
            _completed = true;
            _callback(this);

        }


    }
}
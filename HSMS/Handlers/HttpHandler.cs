﻿//
using System;
using System.IO;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Text;
using Newtonsoft.Json.Linq;

namespace com.bkss.Handlers
{
    public class HttpHandler 
    {
        private const string TAG = "Custom Handler> ";
        private IServiceProvider _provider;
        private IHostingEnvironment _environment;
        public HttpHandler(IServiceProvider provider)
        {
            _environment = provider.GetRequiredService<IHostingEnvironment>();
            _provider = provider;
        }
        public async void ProcessRequest(HttpContext context, MemoryStream mStream)
        {
            HttpRequest Request = context.Request;
            HttpResponse Response = context.Response;
            System.Diagnostics.Debug.WriteLine(TAG + " => " + Request.Method);

            //get query param as name-value pairs
            var opts = Request.Query;
            System.Diagnostics.Debug.WriteLine("Quesry String = " + Request.QueryString);
            if (context.Request.Method == "DELETE")
            {
                System.Diagnostics.Debug.WriteLine(TAG + "^^^^^^^^^^^^^^^ Upload File ...^^^^^^^^^^^");
                System.Diagnostics.Debug.WriteLine("Method = " + context.Request.Method);
                var user = context.User.Identity.Name;
                if (user != null)
                {
                    System.Diagnostics.Debug.WriteLine(TAG + "Upload File for - " + user);
                    //new FileManager().ProcessRequest(context);
                }
                else
                {
                    return;
                }
                System.Diagnostics.Debug.WriteLine("+====================================================+");
            }
            else if (opts.Any(p=>p.Key=="f") && context.User.Identity.IsAuthenticated)
            {
                System.Diagnostics.Debug.WriteLine(TAG + "Download File for - " + context.User.Identity.Name);
                //new FileManager().ProcessRequest(context);
                //Response.Write("[Not Permitted]");  
                System.Diagnostics.Debug.WriteLine(TAG + "Download complete.");
            }
            else
            {
                try
                {
                    //process and write output as text
                    string response = new RequestManager(_provider).handle(context, opts).ToString();
                    System.Diagnostics.Debug.WriteLine(TAG + "Response : " + response);
                    byte[] data = Encoding.UTF8.GetBytes(JToken.Parse(response).ToString());
                    context.Response.ContentType = "application/json";
                    await Response.Body.WriteAsync(data, 0, data.Length);
                }
             catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(TAG + " Parse req. failed.\n"+e.Source);
                Response.StatusCode = 500;
            }

        }
        //Response.Flush();
    }
    
    }
}
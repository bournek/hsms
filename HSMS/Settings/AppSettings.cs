﻿namespace com.bkss.Settings
{
    public class AppRoles
    {
        public const string AppAdminRole = "hsmsAdmin";
        public const string AppStaffRole = "hsmsStaff";
        public const string AppParentRole = "hsmsParent";
        public const string AppStudentRole = "hsmsStudent";
    }
    /// <summary>
    /// The settings for the current application.
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// Gets or sets the short name of the application, used for display purposes where the full name will be too long.
        /// </summary>
        public string SiteShortTitle { get; set; }

        /// <summary>
        /// Gets or sets the full name of the application.
        /// </summary>
        public string SiteTitle { get; set; }
        /// <summary>
        /// Where this site is running
        public Theme Theme { get; set; }
        public Profile Profile { get; set; }
        public string Version { get; set; }
    }

    public class Theme
    {
        /// <summary>
        /// Default theme for application
        /// </summary>
        public string CurrentTheme { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EnableTiles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EnableLoader { get; set; }
        public string ColorPrimary { get; set; }
        public string ColorAccent { get; set; }
    }
    public class Profile
    {
        /// <summary>
        /// Default theme for application
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
    }
}
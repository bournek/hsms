﻿namespace com.bkss.Settings
{
    public class ResponseCompressionSettings
    {
        public string[] MimeTypes { get; set; }
    }
}

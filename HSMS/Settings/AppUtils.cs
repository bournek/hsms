﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.bkss.Settings
{
    public class AppUtils
    {
        public static StringBuilder TrimBuilder(StringBuilder b)
        {
            try
            {
                var raw = b.ToString();

                if (raw.EndsWith("'"))
                {
                    raw = raw.TrimEnd('\'');
                }
                b = new StringBuilder();
                b.Append(raw);

                //
                return b;
            }
            catch
            {
                return new StringBuilder();
            }
        }
        /// <summary>
        /// Replaces '\'' with JSON friendly ` character and ':' with ';'
        /// </summary>
        /// <param name="raw"></param>
        /// <returns></returns>
        public static string SanitizeForJSON(string raw)
        {
            try
            {
                raw = raw.Replace('\'', '`');
                raw = raw.Replace(':', ';');
            }
            catch
            {
                return "";
            }
            return raw;
        }

        private const string _dateFormat = "yyyy-MM-ddTHH:mm:ss.ffzzz";
        /// <summary>
        /// Converts time to ISO-DateTime string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string FormatDate(DateTime date)
        {
            //return date.ToString("o");//2016-09-22T14:01:54.9571247Z
            return date.ToString(_dateFormat);
        }
    }
}

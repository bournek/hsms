﻿namespace HSMS
{
    using System.IO;
    using System.Linq;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using System.Security.Cryptography.X509Certificates;
    using System.Text.RegularExpressions;
    using System;
    using System.Threading.Tasks;

    public sealed class Program
    {
        private const string HostingJsonFileName = "hosting.json";

        public static void Main(string[] args)
        {
            TaskScheduler.UnobservedTaskException += (sender, e) =>
            {
                Console.WriteLine("Unobserved exception: {0}", e.Exception);
                System.Diagnostics.Trace.WriteLine(String.Format("Unobserved exception: {0}", e.Exception));
            };
            //
            var configuration = new ConfigurationBuilder()
                .AddJsonFile(HostingJsonFileName, optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();
            //
            IConfigurationRoot launchConfiguration = null;
            //
            IHostingEnvironment hostingEnvironment = null;

            //var regEx = new Regex(@"((http[s]?):\/\/)([\w\d\.]*)(?:\:\d+)");
            //var rootUrl = regEx.Match(configuration["urls"]).Value;
            string rootUrl = null;
            int rootUrlPort = 0;//new Uri(rootUrl).Port


            var host = new WebHostBuilder()
                //.ConfigureLogging((_, factory) =>
                //{
                //    factory.AddConsole();
                //})
                .UseConfiguration(configuration)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(
                    services =>
                    {
                        hostingEnvironment = services
                            .Where(x => x.ServiceType == typeof(IHostingEnvironment))
                            .Select(x => (IHostingEnvironment)x.ImplementationInstance)
                            .First();

                        //
                        launchConfiguration = new ConfigurationBuilder()
                        .SetBasePath(hostingEnvironment.ContentRootPath)
                        .AddJsonFile(@"Properties\launchSettings.json")
                        .Build();
                        //
                        rootUrl = launchConfiguration.GetValue<string>("iisSettings:iisExpress:applicationUrl");
                        //
                        rootUrlPort = new Uri(rootUrl).Port+1;
                    })
                .UseKestrel(
                    //options =>
                    //{
                    //    // Do not add the Server HTTP header when using the Kestrel Web Server.
                    //    options.AddServerHeader = false;
                    //    if (hostingEnvironment.IsDevelopment())
                    //    {
                    //        // Use a self-signed certificate to enable 'dotnet run' to work in development.
                    //        // This will give a browser security warning which you can safely ignore.
                    //        //options.UseHttps("DevelopmentCertificate.pfx", "password");
                    //    }
                    //}
                    options => options.Listen(System.Net.IPAddress.Loopback, rootUrlPort, listenOptions =>
                    {
                        if (hostingEnvironment.IsDevelopment())
                        {
                            // Run callbacks on the transport thread
                            options.ApplicationSchedulingMode = Microsoft.AspNetCore.Server.Kestrel.Transport.Abstractions.Internal.SchedulingMode.Inline;
                            //
                            options.Listen(System.Net.IPAddress.Loopback, rootUrlPort, opts =>
                            {
                                // Uncomment the following to enable Nagle's algorithm for this endpoint.
                                //listenOptions.NoDelay = false;

                                opts.UseConnectionLogging();
                            });
                            //
                            listenOptions.UseHttps(new X509Certificate2("DevelopmentCertificate.pfx", "password"));

                            //
                            options.UseSystemd();

                            // The following section should be used to demo sockets
                            //options.ListenUnixSocket("/tmp/kestrel-test.sock");
                        }
                    })
                    //
                    )
                    .UseLibuv(options =>
                    {
                        // Uncomment the following line to change the default number of libuv threads for all endpoints.
                        // options.ThreadCount = 4;
                    })
                .UseAzureAppServices()
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
﻿using com.bkss.IdentityModels;
using com.bkss.Settings;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace com.bkss.IdentityModels
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseTasks
    {
        private static ILogger _logger;

        //Admin
        const string name2 = "Bourne_K";
        const string email2 = "bournekolo@yahoo.com";
        const string password2 = "Admin@112";

        /*public static async Task InitializeIdentityDbAsync(IServiceProvider serviceProvider,ILoggerFactory logger)
        {
            using (var db = serviceProvider.GetRequiredService<ApplicationDbContext>().)
            {
                var sqlDb = db.Database;
                if (sqlDb != null)
                {
                    _logger = logger.CreateLogger<DatabaseTasks>();
                    // Create the database if it does not already exist
                    _logger.LogInformation(2,"InitializeIdentityDbAsync: Ensuring Database exists");
                    await sqlDb.EnsureCreatedAsync();
                    // Create the first user if it does not already exist
                    await CreateAdminUser(serviceProvider);
                }
            }
        }*/
        public static async Task InitializeIdentityDatabaseAsync(IServiceProvider serviceProvider, ILoggerFactory logger, IHostingEnvironment env)
        {
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                _logger = logger.CreateLogger<DatabaseTasks>();
                if (await db.Database.EnsureCreatedAsync())
                {
                    // Create the database if it does not already exist
                    _logger.LogInformation(2, "InitializeIdentityDbAsync: Ensuring Database exists");
                    //await sqlDb.EnsureCreatedAsync();
                    // Create the first user if it does not already exist
                    await CreateAdminUser(serviceProvider, env);
                }
                else
                {
                    // Create the database if it does not already exist
                    _logger.LogInformation(2, "InitializeIdentityDbAsync: Database already exists.");
                }
                //
            }
        }
        /// <summary>
        /// Creates a store manager user who can manage the inventory.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        private static async Task CreateAdminUser(IServiceProvider serviceProvider, IHostingEnvironment env)
        {
            //var options = serviceProvider.GetRequiredService<IOptions<IdentityDbContextOptions>>().Value;
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();

            // If the admin role does not exist, create it.
            _logger.LogInformation(2, "CreateAdminUser: Ensuring " + AppRoles.AppAdminRole + " admin exists");
            if (!await roleManager.RoleExistsAsync(AppRoles.AppAdminRole))
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppAdminRole + " does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(AppRoles.AppAdminRole));
                DumpIdentityResult("CreateAdminUser: " + AppRoles.AppAdminRole + " Role Creation", roleCreationResult);
            }
            else
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppAdminRole + " exists");
            }
            //Staff
            _logger.LogInformation(2, "CreateAdminUser: Ensuring " + AppRoles.AppStaffRole + " admin exists");
            if (!await roleManager.RoleExistsAsync(AppRoles.AppStaffRole))
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppStaffRole + " does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(AppRoles.AppStaffRole));
                DumpIdentityResult("CreateAdminUser: " + AppRoles.AppStaffRole + " Role Creation", roleCreationResult);
            }
            else
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppStaffRole + " exists");
            }
            //
            _logger.LogInformation(2, "CreateAdminUser: Ensuring " + AppRoles.AppParentRole + " admin exists");
            if (!await roleManager.RoleExistsAsync(AppRoles.AppParentRole))
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppParentRole + " does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(AppRoles.AppParentRole));
                DumpIdentityResult("CreateAdminUser: " + AppRoles.AppParentRole + " Role Creation", roleCreationResult);
            }
            else
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppAdminRole + " exists");
            }
            //
            _logger.LogInformation(2, "CreateAdminUser: Ensuring " + AppRoles.AppStudentRole + " admin exists");
            if (!await roleManager.RoleExistsAsync(AppRoles.AppStudentRole))
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppStudentRole + " does not exist - creating");
                var roleCreationResult = await roleManager.CreateAsync(new ApplicationRole(AppRoles.AppStudentRole));
                DumpIdentityResult("CreateAdminUser: " + AppRoles.AppStudentRole + " Role Creation", roleCreationResult);
            }
            else
            {
                _logger.LogInformation(2, "CreateAdminUser: Role " + AppRoles.AppStudentRole + " exists");
            }


            // if the user does not exist, create it.
            _logger.LogInformation(2, String.Format("CreateAdminUser: Ensuring User {0} exists", name2));
            var user = await userManager.FindByNameAsync(name2);
            if (user == null)
            {
                _logger.LogInformation(2, "CreateAdminUser: User does not exist - creating");
                user = new ApplicationUser { UserName = name2, Email = email2 };
                user.FullName = "Bourne Koloh";
                user.AccountType = AccountType.Admin;
                //
                var userCreationResult = await userManager.CreateAsync(user, password2);
                DumpIdentityResult("CreateAdminUser: User Creation", userCreationResult);
                if (userCreationResult.Succeeded)
                {
                    _logger.LogInformation(2, "CreateAdminUser: Adding new user to role admin");
                    var roleAdditionResult = await userManager.AddToRoleAsync(user, AppRoles.AppAdminRole);
                    await userManager.AddClaimAsync(user, new Claim("ManageStore", "Allowed"));
                    await userManager.AddClaimAsync(user, new Claim("ManageSystem", "Allowed"));
                    DumpIdentityResult("CreateAdminUser: Role Addition", roleAdditionResult);

                    var admin = new AdminProfile()
                    {
                        ApplicationUserId = user.Id,
                        Type = AdminType.AppAdmin
                    };

                    user.AdminProfile = admin;
                    //
                    await userManager.UpdateAsync(user);

                    using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                    {
                        var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                        var h = db.Users.Include(o => o.AdminProfile).Single(p => p.Id == user.Id);

                        if(h.AdminProfile == null)
                        {
                            _logger.LogError(1, "Create AdminProfile Failed!!");
                            db.Admins.Add(admin);
                            await db.SaveChangesAsync();
                        }
                    }
               }
            }
            else
            {
                _logger.LogInformation(2, "CreateAdminUser: User already exists");
            }

            //ADD COUNTRIES
            using (var serviceScope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var db = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();
                //
                if(!db.Countries.Any(p=>p.Name == "Burundi"))
                {
                    var burundi = new Country()
                    {
                        Name = "Burundi",
                        Code = "+257"
                    };
                    db.Countries.Add(burundi);
                    db.SaveChanges();
                    //
                    try
                    {
                        var data = File.ReadAllText(env.WebRootPath + "/libs/cached/schools-bu.js");
                        CreateSchools(db, data, burundi.Id);
                    }
                    catch { }
                }
                //
                if(!db.Countries.Any(p=>p.Name == "Kenya"))
                {
                    //
                    var kenya = new Country()
                    {
                        Name = "Kenya",
                        Code = "+254"
                    };
                    db.Countries.Add(kenya);
                    db.SaveChanges();
                    //
                    try
                    {
                        var data = File.ReadAllText(env.WebRootPath + "/libs/cached/schools-ke.js");
                        CreateSchools(db, data, kenya.Id);
                    }
                    catch { }
                }
                //
                if(!db.Countries.Any(p=>p.Name == "Tanzania"))
                {
                    var tanzania = new Country()
                    {
                        Name = "Tanzania",
                        Code = "+255"
                    };
                    db.Countries.Add(tanzania);
                    db.SaveChanges();
                    //
                    try
                    {
                        var data = File.ReadAllText(env.WebRootPath + "/libs/cached/schools-tz.js");
                        CreateSchools(db, data, tanzania.Id);
                    }
                    catch { }
                }
                //
                if(!db.Countries.Any(p=>p.Name == "Rwanda"))
                {
                    var rwanda = new Country()
                    {
                        Name = "Rwanda",
                        Code = "+250"
                    };
                    db.Countries.Add(rwanda);
                    db.SaveChanges();
                    //
                    try
                    {
                        var data = File.ReadAllText(env.WebRootPath + "/libs/cached/schools-rw.js");
                        CreateSchools(db, data, rwanda.Id);
                    }
                    catch { }
                }
                //
                if (!db.Countries.Any(p => p.Name == "Uganda"))
                {
                    var uganda = new Country()
                    {
                        Name = "Uganda",
                        Code = "+256"
                    };
                    db.Countries.Add(uganda);
                    db.SaveChanges();
                    //
                    try
                    {
                        var data = File.ReadAllText(env.WebRootPath + "/libs/cached/schools-ug.js");
                        CreateSchools(db, data, uganda.Id);
                    }
                    catch { }
                }
            }
        }

        private static void DumpIdentityResult(string prefix, IdentityResult result)
        {
            _logger.LogInformation(2, String.Format("{0}: Result = {1}", prefix, result.Succeeded ? "Success" : "Failed"));
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    _logger.LogInformation(2, String.Format("--> {0}: {1}", error.Code, error.Description));
                }
            }
        }

        #region Schools Initializers
        private static void CreateSchools(ApplicationDbContext db, string data, int cid)
        {
            var country = db.Countries.Single(p => p.Id == cid);
            //
            var h = Task.Run(async() => {
                List<CountrySchool> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CountrySchool>>(data);
                //
                foreach (var s in list)
                {
                    s.Id = 0;
                    s.CountryId = country.Id;
                    s.Country = country;
                }
                //
                country.Schools = list;
                await db.SaveChangesAsync();
                //
                return 0;

            }).Result;
        }
       
        #endregion
    }
}

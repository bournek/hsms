using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using com.bkss.Settings;

namespace com.bkss.IdentityModels
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        //
        public DbSet<Country> Countries { get; set; }
        public DbSet<CountrySchool> AllSchools { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<SchoolDormitory> Dormitories { get; set; }
        public DbSet<SchoolLibrary> Libraries { get; set; }
        public DbSet<SchoolKitchen> Kitchens { get; set; }
        public DbSet<SchoolLaboratory> Laboratories { get; set; }
        public DbSet<SchoolOffice> Offices { get; set; }
        public DbSet<SchoolHall> Halls { get; set; }
        public DbSet<SchoolClass> Classrooms { get; set; }
        public DbSet<SchoolDepartment> Departments { get; set; }

        ///Co-Curricular
        public DbSet<SchoolEvent> Events { get; set; }
        public DbSet<CoCurricularActivity> CoCurricularEvents { get; set; }

        /// Academics
        public DbSet<AcademicClass> Admissions { get; set; }
        public DbSet<AcademicYear> Years { get; set; }
        public DbSet<AcademicTerm> Terms { get; set; }
        public DbSet<AcademicLevel> Levels { get; set; }
        public DbSet<ClassTimeTable> TimeTables { get; set; }
        public DbSet<AcademicSubject> Subjects { get; set; }
        public DbSet<SubjectLesson> Lessons { get; set; }
        public DbSet<AcademicExam> Exams { get; set; }
        public DbSet<ExamMarksheet> Marksheets { get; set; }
        public DbSet<ExamApproval> Approvals { get; set; }
        public DbSet<ExamGrade> Grades { get; set; }
        public DbSet<ExamType> ExamTypes { get; set; }

        //ADMIN
        public DbSet<AdminProfile> Admins { get; set; }
        public DbSet<StudentProfile> Students { get; set; }
        public DbSet<GuardianProfile> Guardians { get; set; }
        public DbSet<StaffProfile> Staffs { get; set; }

        //
        public DbSet<HSMSAccount> Accounts { get; set; }
        public DbSet<HSMSClient> Clients { get; set; }

        //
        public DbSet<StaffAllowance> Allowances { get; set; }
        public DbSet<RenumerationPackage> RenumerationPackages { get; set; }
        //
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        public ApplicationDbContext() : base(new DbContextOptions<ApplicationDbContext>())
        {
            //Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>().HasMany(e => e.Claims).WithOne().HasForeignKey(e => e.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasMany(e => e.Logins).WithOne().HasForeignKey(e => e.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasMany(e => e.Roles).WithOne().HasForeignKey(e => e.Id).IsRequired().OnDelete(DeleteBehavior.Cascade);



            ///APP 
            builder.Entity<ApplicationUser>().HasOne<AdminProfile>().WithOne(p => p.ApplicationUser).HasForeignKey<AdminProfile>(p => p.ApplicationUserId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasOne<StaffProfile>().WithOne(p => p.ApplicationUser).HasForeignKey<StaffProfile>(p => p.ApplicationUserId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasOne<StudentProfile>().WithOne(p => p.ApplicationUser).HasForeignKey<StudentProfile>(p => p.ApplicationUserId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<ApplicationUser>().HasOne<GuardianProfile>().WithOne(p => p.ApplicationUser).HasForeignKey<GuardianProfile>(p => p.ApplicationUserId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<StudentProfile>().HasOne<GuardianProfile>(p=>p.Guardian).WithMany(p => p.Students).HasForeignKey(p => p.GuardianId).OnDelete(DeleteBehavior.Restrict);

            //Account<->Client
            builder.Entity<HSMSClient>().HasOne<HSMSAccount>().WithOne(p => p.Client).HasForeignKey<HSMSClient>(p => p.AccountId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<HSMSClient>().HasMany<AdminProfile>();//.WithOne(p => p.ApplicationUser).HasForeignKey<GuardianProfile>(p => p.ApplicationUserId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<HSMSClient>().HasMany<School>().WithOne(p => p.Client).HasForeignKey(p => p.ClientId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<Country>().HasMany<CountrySchool>().WithOne(p => p.Country).HasForeignKey(p => p.CountryId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<SchoolDepartment>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<SchoolKitchen>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<SchoolHall>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<SchoolDormitory>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<SchoolStream>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<AcademicYear>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<AcademicLevel>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<ExamType>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<ExamApproval>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<School>().HasMany<AcademicSubject>().WithOne(p => p.School).HasForeignKey(p => p.SchoolId).OnDelete(DeleteBehavior.Cascade);

            //
            builder.Entity<SchoolDepartment>().HasMany<SchoolOffice>(o=>o.Offices).WithOne(p => p.Department).HasForeignKey(p => p.DepartmentId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<SchoolDepartment>().HasMany<SchoolLibrary>(o => o.Libraries).WithOne(p => p.Department).HasForeignKey(p => p.DepartmentId).OnDelete(DeleteBehavior.Cascade);
            //
            //
            builder.Entity<SchoolDepartment>().HasMany<SchoolLaboratory>(o => o.Laboratories).WithOne(p => p.Department).HasForeignKey(p => p.DepartmentId).OnDelete(DeleteBehavior.Cascade);
            //
            builder.Entity<CoCurricularActivity>().HasMany<SchoolEvent>(o => o.Events).WithOne(p => p.Activity).HasForeignKey(p => p.ActivityId).OnDelete(DeleteBehavior.Cascade);

            //
            builder.Entity<SchoolClass>().HasOne<SchoolDepartment>(o=>o.Department).WithMany(p => p.Classrooms).HasForeignKey(p => p.DepartmentId).OnDelete(DeleteBehavior.Cascade);
            
            //Academic Class
            builder.Entity<AcademicClass>().HasMany<StudentProfile>(o => o.Students).WithOne(p => p.Class).HasForeignKey(p => p.ClassId).OnDelete(DeleteBehavior.Cascade);
            //Academic Year
            builder.Entity<AcademicYear>().HasMany<AcademicTerm>(o => o.Terms).WithOne(p => p.Year).HasForeignKey(p => p.YearId).OnDelete(DeleteBehavior.Cascade);
            //Academic Term<->TimeTables
            builder.Entity<AcademicTerm>().HasMany<ClassTimeTable>(o => o.TimeTables).WithOne(p => p.Term).HasForeignKey(p => p.TermId).OnDelete(DeleteBehavior.Cascade);
            //Lessons <-> Timetable
            builder.Entity<ClassTimeTable>().HasMany<SubjectLesson>(o => o.Lessons).WithOne(p => p.TimeTable).HasForeignKey(p => p.TimeTableId).OnDelete(DeleteBehavior.Cascade);
            //Academic Term<->Events
            builder.Entity<AcademicTerm>().HasMany<SchoolEvent>(o => o.Events).WithOne(p => p.Term).HasForeignKey(p => p.TermId).OnDelete(DeleteBehavior.Cascade);
            //Academic Subject<->Exam
            builder.Entity<AcademicSubject>().HasMany<AcademicExam>(o => o.Exams).WithOne(p => p.Subject).HasForeignKey(p => p.SubjectId).OnDelete(DeleteBehavior.Cascade);
            //Academic Subject<->Grades
            builder.Entity<AcademicSubject>().HasMany<ExamGrade>(o => o.Grades).WithOne(p => p.Subject).HasForeignKey(p => p.SubjectId).OnDelete(DeleteBehavior.Cascade);
            //Academic Subject<->MArksheet
            builder.Entity<AcademicSubject>().HasMany<StaffProfile>(o => o.Tutors);//.WithOne(p => p.Term).HasForeignKey(p => p.TermId).OnDelete(DeleteBehavior.Cascade);
            //Academic Exam<->Marksheet
            builder.Entity<AcademicExam>().HasMany<ExamMarksheet>(o => o.Marksheets).WithOne(p => p.Exam).HasForeignKey(p => p.ExamId).OnDelete(DeleteBehavior.Cascade);
            //Marksheet<->Student
            builder.Entity<ExamMarksheet>().HasOne<StudentProfile>(o => o.Student).WithMany(p => p.ExamMarks).HasForeignKey(p => p.StudentId).OnDelete(DeleteBehavior.Cascade);
            //Marksheet<->ExamResults
            builder.Entity<ExamMarksheet>().HasMany<ExamResult>(o => o.Results).WithOne(p => p.Marksheet).HasForeignKey(p => p.MarksheetId).OnDelete(DeleteBehavior.Cascade);

        }
    }
   
}

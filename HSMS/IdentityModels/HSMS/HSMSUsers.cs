﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.IdentityModels
{
    public enum SponsorType
    {
        Parent, Guadian
    }
    public enum StudentType
    {
        DayScholar, Border
    }

    public enum StaffType
    {
        Tutor, NonTeaching
    }
    public enum RenumeartionType
    {
        Salary,Wage
    }
    public enum EmploymentType
    {
        Fulltime,Parttime,Contract
    }
    public class StaffProfile
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public string EmployeeNumber { get; set; }
        public StaffType Type { get; set; }
        public EmploymentType EmploymentType { get; set; }
        public string JobGroup { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual TutorInfo TutorInfo { get; set; }
        public virtual StaffInfo StaffInfo { get; set; }
        public virtual ICollection<CoCurricularActivity> CoCurricular { get; set; }
        public StaffProfile()
        {
            Type = StaffType.Tutor;
        }
    }
    public class StudentProfile
    {
        public int Id { get; set; }
        public int ClassId { get; set; }
        public int ApplicationUserId { get; set; }
        public int GuardianId { get; set; }
        public StudentType Type { get; set; }
        public virtual AcademicLevel Level { get; set; }
        public virtual AcademicClass Class { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual GuardianProfile Guardian { get; set; }
        public virtual ICollection<AcademicSubject> Subjects { get; set; }
        public virtual ICollection<ExamMarksheet> ExamMarks { get; set; }
        public StudentProfile()
        {
            Type = StudentType.Border;
            ExamMarks = new List<ExamMarksheet>();
            Subjects = new List<AcademicSubject>();
        }
    }
    public class GuardianProfile
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public string FullName { get; set; }
        public SponsorType Type { get; set; }
        public virtual ICollection<StudentProfile> Students { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public GuardianProfile()
        {
            Type = SponsorType.Parent;
            Students = new List<StudentProfile>();
        }
    }

    public class TutorInfo
    {
        public int Id { get; set; }
        public int StaffId{ get; set; }
        public virtual StaffProfile Staff { get; set; }
        /// <summary>
        /// E.G TSS
        /// </summary>
        [Required]
        public string EmployerName { get; set; }
        /// <summary>
        /// E.G TSC NUMBER
        /// </summary>
        [Required]
        public string EmployerPin { get; set; }
        public virtual AcademicSubject MajorSubject { get; set; }
        public virtual AcademicSubject MinorSubject { get; set; }
    }
    public class StaffInfo
    {
        public int Id { get; set; }
        public int StaffId { get; set; }
        public virtual StaffProfile Staff { get; set; }
        public virtual RenumerationPackage Renumeration { get; set; }
    }

    public class RenumerationPackage
    {
        public int Id { get; set; }
        public int StaffInfoId { get; set; }
        public RenumeartionType Type { get; set; }
        public double Net { get; set; }
        public virtual ICollection<StaffAllowance> Allowances { get; set; }
        public virtual StaffInfo StaffInfo { get; set; }
    }

    public class StaffAllowance
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public EmploymentType JobType { get; set; }
        public double Percentage { get; set; }
    }
}

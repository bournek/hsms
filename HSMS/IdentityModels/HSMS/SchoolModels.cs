﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.IdentityModels
{
    public enum HallType
    {
        Dining,Other
    }
    public enum DepartmentType
    {
        Academic, Welfare ,Adminstration
    }
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public virtual ICollection<CountrySchool> Schools { get; set; }
        public Country()
        {
            Schools = new List<CountrySchool>();
        }
    }
    public class CountrySchool
    {
        public int Id { get; set; }
        public int CountryId { get; set; }
        public string County { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ContactEmail { get; set; }
        public string Motto { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        public virtual Country Country {get;set;}
    }
    /// <summary>
    /// A subsription my be serving mor than one school
    /// </summary>
    /// 
    public class School
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public virtual HSMSClient Client { get; set; }
        public virtual CountrySchool CountrySchool { get; set; }
        public virtual ICollection<AcademicSubject> Subjects { get; set; }
        public virtual ICollection<ExamType> ExamTypes { get; set; }
        public virtual ICollection<ExamApproval> ExamApprovals { get; set; }
        public virtual ICollection<AcademicYear> Years { get; set; }
        public virtual ICollection<AcademicLevel> Levels { get; set; }
        public virtual ICollection<SchoolStream> Streams { get; set; }
        public virtual ICollection<SchoolDormitory> Dormitories { get; set; }
        public virtual ICollection<SchoolDepartment> Departments { get; set; }
        public virtual ICollection<SchoolKitchen>Kitchens { get; set; }
        public virtual ICollection<SchoolHall> Halls { get; set; }
        public School()
        {
            Streams = new List<SchoolStream>();
            Dormitories = new List<SchoolDormitory>();
            Departments = new List<SchoolDepartment>();
            Kitchens = new List<SchoolKitchen>();
            Halls = new List<SchoolHall>();
        }

    }
    public class SchoolStream
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public virtual School School { get; set; }
    }

    public class SchoolClass{
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public virtual SchoolStream Stream { get; set; }
        public virtual SchoolDepartment Department { get; set; }
    }
    public class SchoolDepartment
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public DepartmentType Type { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<SchoolClass> Classrooms { get; set; }
        public virtual ICollection<SchoolOffice> Offices { get; set; }
        public virtual ICollection<SchoolLaboratory> Laboratories { get; set; }
        public virtual ICollection<SchoolLibrary> Libraries { get; set; }

        public SchoolDepartment()
        {
            Classrooms = new List<SchoolClass>();
            Offices = new List<SchoolOffice>();
            Laboratories = new List<SchoolLaboratory>();
            Libraries = new List<SchoolLibrary>();
        }
    }
    public class SchoolDormitory
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public virtual School School { get; set; }
    }
    public class SchoolOffice
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public virtual SchoolDepartment Department { get; set; }
    }
    public class SchoolLibrary
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public virtual SchoolDepartment Department { get; set; }
    }
    public class SchoolLaboratory
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public virtual SchoolDepartment Department { get; set; }
    }
    public class SchoolKitchen
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public virtual School School { get; set; }
    }
    public class SchoolHall
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public HallType Type { get; set; }
        public virtual School School { get; set; }
    }

    public class SchoolEvent
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public int TermId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Scheduled { get; set; }
        public DateTime Added { get; set; }
        public virtual AcademicTerm Term { get; set; }
        public virtual School School { get; set; }
        public virtual CoCurricularActivity Activity { get; set; }
        public SchoolEvent()
        {
            Added = DateTime.Now;
            Scheduled = DateTime.Now;
        }

    }

    /// <summary>
    /// Co-Curricular Skills like Football, Music, Athletics...
    /// </summary>
    public class CoCurricularActivity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<SchoolEvent> Events { get; set; }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.IdentityModels
{
    public enum AdmissionType
    {
        Dayscholar,Boarding
    }
    public enum FileExtension
    {
        PDF,PNG,JPEG,Other
    }
    /// <summary>
    /// Accademic Admission
    /// </summary>
    public class AcademicClass
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public AdmissionType Type { get; set; }
        public DateTime Started { get; set; }
        public DateTime Ended { get; set; }
        public virtual ICollection<StudentProfile> Students { get; set; }
        public AcademicClass()
        {
            Started = DateTime.Now;
            Type = AdmissionType.Boarding;
            Students = new List<StudentProfile>();
        }
    }
    public class AcademicLevel
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public virtual School School { get; set; }
    }
    public class AcademicYear
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        public virtual School School { get; set; }
        public virtual ICollection<AcademicTerm> Terms { get; set; }

        public AcademicYear()
        {
            Starts = DateTime.Now;
            Ends = DateTime.Now;

            Terms = new List<AcademicTerm>();
        }
    }

    public class AcademicTerm
    {
        public int Id { get; set; }
        public int YearId { get; set; }
        public string Name { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        public virtual AcademicYear Year { get; set; }
        public virtual ICollection<SchoolEvent> Events { get; set; }
        public virtual ICollection<ClassTimeTable> TimeTables { get; set; }
        public AcademicTerm()
        {
            Starts = DateTime.Now;
            Ends = DateTime.Now;
            TimeTables = new List<ClassTimeTable>();
            Events = new List<SchoolEvent>();
        }
    }
    public class AcademicSubject
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool Compulsory { get; set; }
        /// <summary>
        /// Where this subject is offered
        /// </summary>
        public virtual School School { get; set; }//
        /// <summary>
        /// Level/class where this subject is taught e.g form1 , form 2...
        /// </summary>
        public virtual AcademicLevel Level { get; set; }//
        /// <summary>
        /// All exams for this subject
        /// </summary>
        public virtual ICollection<AcademicExam> Exams { get; set; }
        /// <summary>
        /// Collection of tutors who train this subject
        /// </summary>
        public virtual ICollection<StaffProfile> Tutors { get; set; }
        /// <summary>
        /// The grading mechanisim for this subject
        /// </summary>
        public virtual ICollection<ExamGrade> Grades { get; set; }
        public AcademicSubject()
        {
            Exams = new List<AcademicExam>();
            Tutors = new List<StaffProfile>();
        }
    }
    public class ClassTimeTable
    {
        public int Id { get; set; }
        public int TermId { get; set; }
        /// <summary>
        /// Uploaded binary file
        /// </summary>
        public byte[] Value { get; set; }
        public string Path { get; set; }
        public string ContentType { get; set; }
        public virtual AcademicTerm Term { get; set; }
        public virtual ICollection<SubjectLesson> Lessons { get; set; }
        public ClassTimeTable()
        {
            Lessons = new List<SubjectLesson>();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SubjectLesson
    {
        public int Id { get; set; }
        public int TimeTableId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public string Icon { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        public bool IsFullDay { get; set; }
        public virtual SchoolClass Classroom { get; set; }
        public virtual AcademicSubject Subject { get; set; }
        public virtual ClassTimeTable TimeTable { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class AcademicExam
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        /// <summary>
        /// This exam is mandatory or not
        /// </summary>
        public bool Required { get; set; }
        public virtual AcademicSubject Subject { get; set; }
        public virtual ICollection<ExamMarksheet> Marksheets { get; set; }
        public virtual ExamType Type { get; set; }
        public AcademicExam()
        {
            Marksheets = new List<ExamMarksheet>();
        }
    }
    public class ExamMarksheet
    {
        public int Id { get; set; }
        public int ExamId { get; set; }
        public int StudentId { get; set; }
        /// <summary>
        /// the student who did the exam
        /// </summary>
        public virtual StudentProfile Student { get; set; }//
        /// <summary>
        /// the exam<->subject
        /// </summary>
        public virtual AcademicExam Exam { get; set; }//
        /// <summary>
        /// Aggregate scored grade e.g A,B...
        /// Available when results are available and are RELEASED
        /// </summary>
        public virtual ExamGrade Grade { get; set; }//
        /// <summary>
        /// If subject has more than one exam
        /// </summary>
        public virtual ICollection<ExamResult> Results { get; set; }//
        public ExamMarksheet()
        {
            Results = new List<ExamResult>();
        }
    }
    public class ExamResult
    {
        public int Id { get; set; }
        public int MarksheetId { get; set; }
        public double Value { get; set; }
        public virtual ExamApproval Approval { get; set; }
        public virtual ExamMarksheet Marksheet { get; set; }
    }
    /// <summary>
    /// To be configured by users
    /// </summary>
    public class ExamGrade
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        /// <summary>
        /// Min score for this grade '>='
        /// </summary>
        public double MinScore { get; set; }//
        /// <summary>
        /// Equivalent value for score e.g A., B+, ...
        /// </summary>
        public string Label { get; set; }//
        /// <summary>
        /// Eg. Excelent, Good, ...
        /// </summary>
        public string Remarks { get; set; }
        public virtual AcademicSubject Subject { get; set; }
        /// <summary>
        /// Where this grade should be applied, eg grading form 1,form 2.. results
        /// </summary>
        public virtual ICollection<AcademicLevel> Levels { get; set; }
        public ExamGrade()
        {
            Levels = new List<AcademicLevel>();
        }
    }
    /// <summary>
    /// To be configured by uses
    /// </summary>
    public class ExamType
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        /// <summary>
        /// E.G Continous Assesment Test
        /// </summary>
        public string Name { get; set; }//
        /// <summary>
        /// E.G CAT, RAT
        /// </summary>
        public string Alia{ get; set; }//
        /// <summary>
        /// Expected total marks
        /// </summary>
        public double TotalScore { get; set; }//
        /// <summary>
        /// E.G 0.5
        /// </summary>
        public double ConversionConstant { get; set; }//
        public virtual School School { get; set; }
    }
   /// <summary>
   /// To be configured by uses
   /// </summary>
    public class ExamApproval
    {
        public int Id { get; set; }
        public int SchoolId { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }
        public bool IsReleased { get; set; }
        public virtual School School { get; set; }
    }
}

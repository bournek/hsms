﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.IdentityModels
{
    public enum HSMSAccountStatus
    {
        Pending,Expired,Suspended,Activated
    }
    /// <summary>
    /// Contains app secrets. Used to communicate with bkss.co.ke server for validations
    /// </summary>
    public class HSMSAccount
    {
        public int Id { get; set; }
        public string ConsumerId { get; set; }
        public string ConsumerSecret { get; set; }
        public string Token { get; set; }
        public DateTime Activated { get; set; }
        public DateTime Validated { get; set; }
        public HSMSAccountStatus Status { get; set; }
        public virtual HSMSClient Client { get; set; }
    }
    public class HSMSClient
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        public virtual Country Country { get; set; }
        public string State { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }
        public virtual HSMSAccount Account { get; set; }
        public virtual ICollection<School> Schools { get; set; }
        public virtual ICollection<AdminProfile> Admins { get; set; }

        public HSMSClient()
        {
            Schools = new List<School>();
            Admins = new List<AdminProfile>();
        }
    }
}

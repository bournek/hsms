using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace com.bkss.IdentityModels
{
    public enum AccountType
    {
       Student,Staff,Guardian,Admin
    }
    public enum AdminType
    {
        SchoolAdmin,ClientAdmin, AppAdmin
    }
   
    public enum Gender
    {
        Male = 0, Female = 1, Unspecified = 2
    }
    //public class ApplicationUserLogin : IdentityUserLogin<int>
    //{
    //    public virtual ApplicationUser ApplicationUser { get; set; }
    //}


    //public class ApplicationUserRole : IdentityUserRole<int>
    //{
    //    public virtual ApplicationUser ApplicationUser { get; set; }
    //}


    //public class ApplicationUserClaim : IdentityUserClaim<int>
    //{
    //    public virtual ApplicationUser ApplicationUser { get; set; }
    //}
    public class ApplicationRole : IdentityRole<int>
    {
        public string Description { get; set; }

        public int Level { get; set; }
        public int Alias { get; set; }
        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : this()
        {
            this.Name = name;
        }

        public ApplicationRole(string name, string description) : this(name)
        {
            this.Description = description;
        }
        public ApplicationRole(string name, int level) : this(name)
        {
            this.Level = level;
        }
    }
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<int>
    {
        public AccountType AccountType { get; set; }
        public Gender Gender { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastSeen { get; set; }
        public string FullName { get; set; }
        public AdminProfile AdminProfile { get; set; }
        public StudentProfile StudentProfile { get; set; }
        public StaffProfile StaffProfile { get; set; }

        ///REMOVED IN .NET 2.0
        
        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<ApplicationRole> Roles { get; } = new List<ApplicationRole>();

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<IdentityUserClaim<int>> Claims { get; } = new List<IdentityUserClaim<int>>();

        /// <summary>
        /// Navigation property for this users login accounts.
        /// </summary>
        public virtual ICollection<IdentityUserLogin<int>> Logins { get; } = new List<IdentityUserLogin<int>>();
        public ApplicationUser()
        {
            Gender = Gender.Unspecified;
            DateCreated = DateTime.Now;
        }
    }

    public class AdminProfile
    {
        public int Id { get; set; }
        public int ApplicationUserId { get; set; }
        public AdminType Type { get; set; }
        public virtual ApplicationUser ApplicationUser {get;set;}
        public AdminProfile()
        {
            Type = AdminType.SchoolAdmin;
        }
    }
}

﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace com.bkss.Handlers
{
    
    public class HandlerMiddleware
    {
        private const string TAG = "Handler MW > ";
        RequestDelegate _next;
        private System.AsyncCallback callback;
        private static IServiceProvider _provider;
        public HandlerMiddleware(RequestDelegate next, IServiceProvider provider)
        {
            _next = next;
            _provider = provider;
        }

        public async Task Invoke(HttpContext context)
        {
            System.Diagnostics.Debug.WriteLine("Invoke Middleware....");
            var sw = new Stopwatch();
            sw.Start();
            System.Diagnostics.Debug.WriteLine("context.Request.Path.ToUriComponent() = "+ context.Request.Path.ToUriComponent());
            using (var memoryStream = new MemoryStream())
            {
                var bodyStream = context.Response.Body;
                //Check custom extension .jsp
                //if (context.Request.Path.ToUriComponent().Contains(".jsp") && context.Request.ContentType == "application/json")
                if (context.Request.Path.ToUriComponent().Contains(".jsp"))
                {
                    switch (context.Request.Method)
                    {
                        case "POST":
                            //
                            System.Diagnostics.Debug.WriteLine(TAG + "JSON post...");
                            new AsyncHttpHandler(_provider).BeginProcessRequest(context,callback, memoryStream);
                            break;
                        case "GET":
                            //
                            System.Diagnostics.Debug.WriteLine(TAG + "JSON get...");
                            System.Diagnostics.Debug.WriteLine("Custom Handle : GET");
                            new HttpHandler(_provider).ProcessRequest(context, memoryStream);
                            break;
                        case "PUT":
                            //
                            System.Diagnostics.Debug.WriteLine(TAG + "JSON put...");
                            System.Diagnostics.Debug.WriteLine("Custom Handle : POST");
                            new AsyncHttpHandler(_provider).BeginProcessRequest(context, callback, memoryStream);
                            break;
                    }
                }
                else
                {
                    //Not ours, procceed
                    await _next(context);
                }

                //Laiter
                var isHtml = context.Response.ContentType?.ToLower().Contains("text/html");
                if (context.Response.StatusCode == 200 && isHtml.GetValueOrDefault())
                {
                    //memoryStream.Seek(0, SeekOrigin.Begin);
                    //using (var streamReader = new StreamReader(memoryStream))
                    //{
                    //    //
                    //    context.Response.OnStarting(state => {
                    //        var httpContext = (HttpContext)state;
                    //        httpContext.Response.Headers.Add("X-ElapsedTime", new[] { sw.ElapsedMilliseconds.ToString() });
                    //        return Task.FromResult(0);
                    //    }, context);

                    //    var responseBody = await streamReader.ReadToEndAsync();
                    //    //var newFooter = @"<footer><div id=""process"">Page processed in {0} milliseconds.</div>";
                    //    // responseBody = responseBody.Replace("<footer>", string.Format(newFooter, sw.ElapsedMilliseconds));
                    //    //context.Response.Headers.Add("X-ElapsedTime", new[] { sw.ElapsedMilliseconds.ToString() });
                    //    using (var amendedBody = new MemoryStream())
                    //    using (var streamWriter = new StreamWriter(amendedBody))
                    //    {
                    //        streamWriter.Write(responseBody);
                    //        amendedBody.Seek(0, SeekOrigin.Begin);
                    //        await amendedBody.CopyToAsync(bodyStream);
                    //    }
                    //}
                }
            }
            System.Diagnostics.Debug.WriteLine("Exit Middleware....");
        }
    }
    
}

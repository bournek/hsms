﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.Middlewares.Utils
{
    public class ProfileKeys
    {
        public const string JSON_KEY_ID = "gid";
        public const string JSON_KEY_DEVICE_ID = "did";
        public const string JSON_KEY_USER_NAME = "name";
        public const string JSON_KEY_OS = "os";
        public const string JSON_KEY_CURR_LOC = "loc";
    }
}

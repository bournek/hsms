﻿namespace com.bkss.Constants
{
    public static class SetupsControllerAction
    {
        public const string Index = "Index";
        public const string Start = "Start";
        public const string Schools = "Schools";
        public const string Staff = "Staff";
        public const string Tution = "Tution";
        public const string Finance = "Finance";
        public const string Ammenity = "Ammenity";
        public const string ChooseSchool = "ChooseSchool";
    }
}
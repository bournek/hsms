﻿namespace com.bkss.Constants
{
    public static class SetupsControllerRoute
    {
        public const string GetIndex = ControllerName.Setups + "GetIndex";
        public const string GetStart = ControllerName.Setups + "GetStart";
        public const string GetSchools = ControllerName.Setups + "GetSchools";
        public const string AddSchool = ControllerName.Setups + "AddSchool";
        public const string AddOffice = ControllerName.Setups + "AddOffice";
        public const string AddClassroom = ControllerName.Setups + "AddClassroom";
        public const string AddDepartment = ControllerName.Setups + "AddDepartment";
        public const string AddStream = ControllerName.Setups + "AddStream";
        public const string GetStaff = ControllerName.Setups + "GetStaff";
        public const string GetAcademics = ControllerName.Setups + "GetAcademics";
        public const string AddYear = ControllerName.Setups + "AddYear";
        public const string AddLevel = ControllerName.Setups + "AddLevel";
        public const string AddTerm = ControllerName.Setups + "AddTerm";
        public const string AddExamType = ControllerName.Setups + "AddExamType";
        public const string AddExamGrade = ControllerName.Setups + "AddExamGrade";
        public const string AddSubject = ControllerName.Setups + "AddSubject";
        public const string AddExam = ControllerName.Setups + "AddExam";
        public const string AddStaff = ControllerName.Setups + "AddStaff";
        public const string AddStudent = ControllerName.Setups + "AddStudent";
        public const string GetFinance = ControllerName.Setups + "GetFinance";
        public const string GetAmmenities = ControllerName.Setups + "GetAmmenities";
        public const string AddTimetable = ControllerName.Setups + "AddTimeTable";
        public const string AddLessons = ControllerName.Setups + "AddLessons";
        public const string UploadStudents = ControllerName.Setups + "UploadStudents";
        public const string GetBundle = ControllerName.Setups + "GetBundle";
        public const string ChooseSchool = ControllerName.Setups + "ChooseSchool";
    }
}
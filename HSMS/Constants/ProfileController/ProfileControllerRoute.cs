﻿namespace com.bkss.Constants
{
    public static class ProfileControllerRoute
    {
        public const string GetProfile = ControllerName.Profile + "GetIndex";
        public const string GetInbox = ControllerName.Profile + "GetInbox";
        public const string GetCalendar = ControllerName.Profile + "GetCalendar";
    }
}
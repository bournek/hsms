﻿namespace com.bkss.Constants
{
    public static class ProfileControllerAction
    {
        public const string Profile = "Index";
        public const string Inbox = "Inbox";
        public const string Calendar = "Calendar";
    }
}
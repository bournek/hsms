﻿namespace com.bkss.Constants
{
    public static class ControllerName
    {
        public const string Error = "Error";
        public const string Home = "Home";
        public const string Account = "Account";
        public const string Profile = "Profile";
        public const string Setups = "Setups";
        public const string Admin = "Admin";
    }
}
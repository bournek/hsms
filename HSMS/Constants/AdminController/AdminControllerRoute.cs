﻿namespace com.bkss.Constants
{
    public static class AdminControllerRoute
    {
        public const string GetIndex = ControllerName.Admin + "Get" + AdminControllerAction.Index;
        public const string GetRoles = ControllerName.Admin + "Get" + AdminControllerAction.Roles;
        public const string GetUsers = ControllerName.Admin + "Get" + AdminControllerAction.Users;
    }
}
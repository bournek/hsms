﻿namespace com.bkss.Constants
{
    public static class AdminControllerAction
    {
        public const string Index = "Index";
        public const string Users = "Users";
        public const string Roles = "Roles";
    }
}
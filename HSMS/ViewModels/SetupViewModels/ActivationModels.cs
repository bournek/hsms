﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class ActivateViewModel
    {
        [Required]
        public string Code { get; set; }
    }
}

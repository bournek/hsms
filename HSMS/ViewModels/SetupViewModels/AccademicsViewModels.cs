﻿using com.bkss.IdentityModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class AddYearViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Starts { get; set; }
        [Required]
        public DateTime Ends { get; set; }
    }
    
    public class AddTermViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int Year { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Starts { get; set; }
        [Required]
        public DateTime Ends { get; set; }
    }

    public class AddSubjectViewModel
    {
        public int Id { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int Department { get; set; }
        [Required]
        public int[] Levels { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool Compulsory { get; set; }
    }
    public class AddExamViewModel
    {
        [Required]
        public int Type { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int Subject { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool Compulsory { get; set; }
    }
    public class AddExamTypeViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int Total { get; set; }
        [Required]
        public string Conversion { get; set; }
    }
    public class AddExamGradeViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int Subject { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name="Cut-Off Marks")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public double MinMarks { get; set; }
        public string Comments { get; set; }
    }
    public class AddTimeTableViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int TermId { get; set; }
        public AcademicTerm Term { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int SubjectId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field has invalid data")]
        public int LevelId { get; set; }
        [Required]
        public string Lessons { get; set; }
        public DateTime Starts { get; set; }
        public DateTime Ends { get; set; }
        //[Required]
        [Display(Name ="Attachment")]
        //[FileExtensions(Extensions = "jpg,jpeg,png,pdf")]
        public IFormFile Doc { get; set; }
        public AddTimeTableViewModel()
        {
            Starts = DateTime.Now;
            Ends = DateTime.Now.AddHours(1);
        }
    }
}

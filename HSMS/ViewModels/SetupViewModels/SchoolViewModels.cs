﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class AddSchoolViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "The School Field is required")]
        public int School { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field is required")]
        public int Country { get; set; }
        public string County { get; set; }
        [Required]
        public string Code { get; set; }
        public string Motto { get; set; }
        public string WorkPhone { get; set; }
        public string MobilePhone { get; set; }
        [Required]
        public string Email { get; set; }
        public string Street { get; set; }
        [Required]
        public string ContactName { get; set; }
        public string City { get; set; }
        [Required]
        public string PostalCode { get; set; }
    }
    public class AddLevelViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public int Level { get; set; }
        [Required]
        public string Name { get; set; }
    }
    public class AddClassViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "This Field is required")]
        public int Department { get; set; }
        [Required]
        public int Stream { get; set; }
    }
    public class AddDepartmentViewModel
    {
        [Required]
        public string DepartmentName { get; set; }
        [Required]
        public int Type { get; set; }
    }
    public class AddStreamViewModel
    {
        [Required]
        public string StreamName { get; set; }
    }
    public class AddOfficeViewModel
    {
        [Required]
        public string Name { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "This Field is required")]
        public int Department { get; set; }
    }
}

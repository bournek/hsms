﻿using com.bkss.IdentityModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{

    public class AddStaffViewModel
    {
        [Required]
        public int SchoolId { get; set; }

        //STUDENT PROFILE
        [Required]
        [Display(Name = "SurName")]
        public string Surname { get; set; }
        [Required]
        [Display(Name = "MiddleName  GivenName")]
        public string OtherNames { get; set; }
        public int Gender { get; set; }
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Profile Pic
        /// </summary>
        public IFormFile Photo { get; set; }
        public bool IsDisabled { get; set; }
        public string Disability { get; set; }

        //Sponsor
        [Required]
        [Display(Name = "Sponsor")]
        public string SponsorName { get; set; }
        [Display(Name = "Other Names")]
        public string SponsorOtherNames { get; set; }
        [Required]
        public int Sponsor { get; set; }
        [Display(Name = "Passport/ID")]
        public string Identifier { get; set; }
        public int SponsorGender { get; set; }
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Occupation { get; set; }
        [Required]
        public int Country { get; set; }
        [Required]
        public string Town { get; set; }
        [Required]
        public string PostalCode { get; set; }
        public string Street { get; set; }

        //Primary School
        [Required]
        public string PrimarySchool { get; set; }
        [Required]
        public int PrimarySchoolCountry { get; set; }
        [Required]
        public int PrimarySchoolCounty { get; set; }
        [Required]
        public string PrimarySchoolAddress { get; set; }

        //ADMIN DETAILS
        public int SchoolAdmitted { get; set; }
        public int LevelAdmitted { get; set; }
        public int StreamAdmitted { get; set; }
        [Display(Name = "Admission Number")]
        public string RegNo { get; set; }

        public AddStaffViewModel()
        {
            DateOfBirth = DateTime.Now.AddYears(-18);
        }

    }
    public class AddStudentViewModel
    {
        [Required]
        public int SchoolId { get; set; }

        //STUDENT PROFILE
        [Required]
        [Display(Name = "SurName")]
        public string Surname { get; set; }
        [Required]
        [Display(Name ="MiddleName  GivenName")]
        public string OtherNames { get; set; }
        public int Gender { get; set; }
        [Display(Name ="Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Profile Pic
        /// </summary>
        public IFormFile Photo { get; set; }
        public bool IsDisabled { get; set; }
        public string Disability { get; set; }

        //Sponsor
        [Required]
        [Display(Name = "Sponsor")]
        public string SponsorName { get; set; }
        [Display(Name = "Other Names")]
        public string SponsorOtherNames { get; set; }
        [Required]
        public int Sponsor { get; set; }
        [Display(Name = "Passport/ID")]
        public string Identifier { get; set; }
        public int SponsorGender { get; set; }
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Occupation { get; set; }
        [Required]
        public int Country { get; set; }
        [Required]
        public string Town { get; set; }
        [Required]
        public string PostalCode { get; set; }
        public string Street { get; set; }

        //Primary School
        [Required]
        public string PrimarySchool { get; set; }
        [Required]
        public int PrimarySchoolCountry { get; set; }
        [Required]
        public int PrimarySchoolCounty { get; set; }
        [Required]
        public string PrimarySchoolAddress { get; set; }

        //ADMIN DETAILS
        public int SchoolAdmitted { get; set; }
        public int LevelAdmitted { get; set; }
        public int StreamAdmitted { get; set; }
        [Display(Name = "Admission Number")]
        public string RegNo { get; set; }
        public AddStudentViewModel()
        {
            DateOfBirth = DateTime.Now.AddYears(-18);
        }

    }
    public class AddTutorViewModel
    {
        [Required]
        public int SchoolId { get; set; }

        //STUDENT PROFILE
        [Required]
        [Display(Name = "SurName")]
        public string Surname { get; set; }
        [Required]
        [Display(Name = "MiddleName  GivenName")]
        public string OtherNames { get; set; }
        public int Gender { get; set; }
        [Display(Name = "Date Of Birth")]
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Profile Pic
        /// </summary>
        public IFormFile Photo { get; set; }
        public bool IsDisabled { get; set; }
        public string Disability { get; set; }

        //Sponsor
        [Required]
        [Display(Name = "Sponsor")]
        public string SponsorName { get; set; }
        [Display(Name = "Other Names")]
        public string SponsorOtherNames { get; set; }
        [Required]
        public int Sponsor { get; set; }
        [Display(Name = "Passport/ID")]
        public string Identifier { get; set; }
        public int SponsorGender { get; set; }
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Occupation { get; set; }
        [Required]
        public int Country { get; set; }
        [Required]
        public string Town { get; set; }
        [Required]
        public string PostalCode { get; set; }
        public string Street { get; set; }

        //Primary School
        [Required]
        public string PrimarySchool { get; set; }
        [Required]
        public int PrimarySchoolCountry { get; set; }
        [Required]
        public int PrimarySchoolCounty { get; set; }
        [Required]
        public string PrimarySchoolAddress { get; set; }

        //ADMIN DETAILS
        public int SchoolAdmitted { get; set; }
        public int LevelAdmitted { get; set; }
        public int StreamAdmitted { get; set; }
        [Display(Name = "Admission Number")]
        public string RegNo { get; set; }

        public AddTutorViewModel()
        {
            DateOfBirth = DateTime.Now.AddYears(-18);
        }

    }
    public class AddStaffRolesViewModel
    {
        [Required]
        public int StaffId { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string RoleName { get; set; }
        [Required]
        [Display(Name = "Alias")]
        public string RoleAlias { get; set; }
        [Display(Name = "Alias")]
        public AccountType UserType { get; set; }
        [Display(Name = "Level")]
        public int RoleLevel { get; set; }
        [Display(Name = "Description")]
        public string RoleDescription { get; set; }
        

        public AddStaffRolesViewModel()
        {
        }

    }
}

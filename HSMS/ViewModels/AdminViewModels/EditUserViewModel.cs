﻿using com.bkss.IdentityModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class EditUserViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public IEnumerable<SelectListItem> Roles { get; set; }
    }
}

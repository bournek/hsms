﻿using com.bkss.IdentityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class UsersViewModel
    {
        public ICollection<int> Users { get; set; }
        public ICollection<ApplicationRole> Roles { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace com.bkss.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email")]
        [Compare("Password", ErrorMessage = "The email and confirmation email do not match.")]
        public string ConfirmEmail { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Account Type")]
        public string Intrest { get; set; }

        [Display(Name = "Intrests")]
        public IEnumerable<SelectListItem> IntrestList { get; set; }
        [Required]
        [Display(Name = "Category")]
        public string IntrestCategory { get; set; }

        [Required]
        [Display(Name = "Office Phone")]
        public string OfficePhone { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Country Codes")]
        public IEnumerable<SelectListItem> CountryCodeList { get; set; }
        [Required]
        [Display(Name = "Country Code")]
        public string CountryCode { get; set; }

        [Required]
        [Display(Name = "Wireless Number")]
        public string WirelessNum { get; set; }

        [Required]
        [Display(Name = "Asigned Name")]
        public string Alias { get; set; }

        [Display(Name = "Select Gender")]
        public IEnumerable<SelectListItem> GenderList { get; set; }
        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }
        public string Salutation { get; set; }
        [Required]
        [Display(Name = "Verify Code")]
        public string VerifyCode { get; set; }

        [Required]
        [Display(Name = "I understand and agree to comply with BKSS terms and conditions.")]
        public bool TermsAccepted { get; set; }

        [Required]
        [Display(Name = "I would also like to get BKSS monthly newsletter")]
        public bool NewsLetterAccepted { get; set; }
    }
}
